(function (app) {
  'use strict';

  // The core module is required for special route handling;
  // see /core/client/config/core.client.routes
  app.registerModule('browse', ['core']);
  app.registerModule('browse.services');
  app.registerModule('browse.routes', ['ui.router', 'core.routes', 'browse.services']);
}(ApplicationConfiguration));
