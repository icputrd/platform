(function () {
  'use strict';

  angular
    .module('browse')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Browse',
      state: 'browse',
      type: 'dropdown',
      roles: ['*']
    });

		// Add dropdown list item
    menuService.addSubMenuItem('topbar', 'browse', {
      title: 'Browse Images',
      state: 'browse.donors',
      roles: ['*']
    });
  }
}());
