(function () {
  'use strict';

  angular
    .module('browse.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('browse', {
        abstract: true,
        url: '/browse',
        template: '<ui-view/>'
      })
      .state('browse.donors', {
        url: '',
        templateUrl: '/modules/browse/client/views/donors-list.client.view.html',
        controller: 'BrowseListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Browse'
        }
      })
      .state('browse.days', {
        url: '/:browseId',
        templateUrl: '/modules/browse/client/views/days-list.client.view.html',
        controller: 'DaysListController',
        controllerAs: 'vm',
        resolve: {
          dayResolve: getDays
        },
        data: {
          pageTitle: 'Browse Days'
        }
      })
      .state('browse.images', {
        url: '/:browseId/:date',
        templateUrl: '/modules/browse/client/views/image-list.client.view.html',
        controller: 'ImagesListController',
        controllerAs: 'vm',
        resolve: {
          imagesResolve: getImages
        },
        data: {
          pageTitle: 'Browse Images'
        }
      })
      .state('browse.view', {
        url: '/:browseId/:date/:num',
        templateUrl: '/modules/browse/client/views/image-view.client.view.html',
        controller: 'ImageViewController',
        controllerAs: 'vm',
        resolve: {
          imageResolve: getImage
        },
        data: {
          pageTitle: 'Image'
        }
      });
  }

  getDays.$inject = ['$stateParams', 'DaysService'];

  function getDays($stateParams, DaysService) {
    console.log('in get days');
    return DaysService.query({
      browseId: $stateParams.browseId
    }).$promise;
  }


  getImages.$inject = ['$stateParams', 'ImagesService'];

  function getImages($stateParams, ImagesService) {
    console.log('in get images');
    return ImagesService.query({
      date: $stateParams.date,
      browseId: $stateParams.browseId
    }).$promise;
  }

  getImage.$inject = ['$stateParams', 'ImageService'];

  function getImage($stateParams, ImageService) {
    return ImageService.query({
      date: $stateParams.date,
      browseId: $stateParams.browseId,
      num: $stateParams.num
    }).$promise;
  }
}());
