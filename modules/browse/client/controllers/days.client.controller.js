(function () {
  'use strict';

  angular
    .module('browse')
    .controller('DaysListController', DaysListController);

  DaysListController.$inject = ['dayResolve', '$scope'];

  function DaysListController(days, $scope) {
    console.log('days controller');
    var vm = this;
    vm.days = days;
    vm.orderProp = 'date';
    // vm.days = DaysService.query();
    console.log('vm.days');
    console.log(vm.days);
  }

}());
