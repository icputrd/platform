(function () {
  'use strict';

  angular
    .module('browse')
    .controller('ImagesListController', ImagesListController);

  ImagesListController.$inject = ['imagesResolve', '$scope'];

  function ImagesListController(images, $scope) {
    console.log('image controller');
    var vm = this;
    vm.images = images;
    console.log(vm.images);
  }

}());
