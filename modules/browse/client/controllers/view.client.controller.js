(function () {
  'use strict';

  angular
    .module('browse')
    .controller('ImageViewController', ImageViewController);

  ImageViewController.$inject = ['imageResolve', '$scope', '$stateParams'];

  function ImageViewController(image, $scope, $stateParams) {
    console.log('image controller');
    var vm = this;
    var num = $stateParams.num;
    if (num < 10) {
      num = num.slice(-1);
    }
    var arr = image[0].days[0].images[num - 1];
    vm.image = arr;
  }

}());
