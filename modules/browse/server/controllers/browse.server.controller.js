'use strict';

var path = require('path'),
  mongoose = require('mongoose'),
  Donors = mongoose.model('Donors'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * List of Donors
 */
exports.list = function (req, res) {
  Donors.find({}, { '_id': 1, 'icon': 1, 'numDays': 1, 'totalPhotos': 1 }).exec(function (err, browse) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      console.log(browse);
      res.json(browse);
    }
  });
};

/**
 * Days list for Donor
 */
exports.days = function (req, res) {
  console.log(req.url);
  var id = req.url.slice(-3);
  console.log(id);
  Donors.find({ '_id': id }, { 'days.date': 1, 'days.icon': 1, 'days.numPhotos': 1 }).exec(function (err, days) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      console.log(days);
      res.json(days);
    }
  });
};

/**
 * Image list for Day
 */
exports.images = function (req, res) {
  console.log(req.url);
  var date = req.url.slice(-5);
  var id = req.url.slice(12, 15);
  console.log(date);
  console.log(id);
  Donors.find({ '_id': id, 'days.date': date }, { 'days.$': 1 }).exec(function (err, images) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      console.log(images);
      res.json(images);
    }
  });
};

/**
 * Single image
 */
exports.view = function (req, res) {
  console.log(req.url);
  var date = req.url.slice(16, 21);
  var id = req.url.slice(12, 15);
  var num = req.url.slice(-2);
  console.log(date);
  console.log(id);
  console.log(num);
  Donors.find({ '_id': id, 'days': { '$elemMatch': { 'date': date, 'images': { '$elemMatch': { 'num': num } } } } }, { 'days.images.$': 1 }).exec(function (err, image) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(image);
    }
  });
};
