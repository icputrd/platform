'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

acl = new acl(new acl.memoryBackend());

/**
 * Invoke Browse Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/browse',
      permissions: '*'
    }, {
      resources: '/api/browse/:donorId',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/browse',
      permissions: '*'
    }, {
      resources: '/api/browse/:donorId',
      permissions: '*'
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/browse',
      permissions: '*'
    }, {
      resources: '/api/browse/:donorId',
      permissions: '*'
    }]
  }]);
};

/**
  * Check If Articles Policy Allows
  */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
