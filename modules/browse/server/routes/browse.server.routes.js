'use strict';

/**
 * Module dependencies
 */
var browsePolicy = require('../policies/browse.server.policy'),
  browse = require('../controllers/browse.server.controller');

module.exports = function (app) {
  // Donor collection routes
  app.route('/api/browse').all(browsePolicy.isAllowed)
    .get(browse.list);

  // Single donor routes - Days collection routes
  app.route('/api/browse/:browseId').get(browse.days);

  // Single day routes - Image collection routes
  app.route('/api/browse/:browseId/:date').get(browse.images);

  // Single image routes
  app.route('/api/browse/:browseId/:date/:num').get(browse.view);

};
