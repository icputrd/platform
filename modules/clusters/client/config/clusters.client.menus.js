(function () {
  'use strict';

  angular
    .module('clusters')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Clusters',
      state: 'clusters',
      type: 'dropdown',
      roles: ['*']
    });

		// Add dropdown list item
    menuService.addSubMenuItem('topbar', 'clusters', {
      title: 'Browse clusters',
      state: 'clusters.labels',
      roles: ['*']
    });
  }
}());
