(function () {
  'use strict';

  angular
    .module('clusters.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('clusters', {
        abstract: true,
        url: '/clusters',
        template: '<ui-view/>'
      })
      .state('clusters.labels', {
        url: '',
        templateUrl: '/modules/clusters/client/views/labels-list.client.view.html',
        controller: 'ClusterListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Cluster Labels'
        }
      })
      .state('clusters.images', {
        url: '/:label',
        templateUrl: '/modules/clusters/client/views/image-list.client.view.html',
        controller: 'ClusterImagesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Cluster Images'
        }
      });
  }
}());
