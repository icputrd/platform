(function () {
  'use strict';

  angular
    .module('clusters')
    .controller('ClusterImagesListController', ClusterImagesListController);

  ClusterImagesListController.$inject = ['ClusterImagesService', '$stateParams', '$scope', '$http'];

  function ClusterImagesListController(ClusterImagesService, $stateParams, $scope, $http) {
    var vm = this;
    vm.images = ClusterImagesService.query({
    	label: $stateParams.label
    });
    vm.selected = [];
    console.log(vm.images);
    vm.labels = ["arm", "back", "backside", "foot", "fullbody", "hand", "head", "hips", "knee", "legs", "other", "plastic", "remove", "shade", "stake", "test", "torso"];
    $scope.changeLabel = function(i) {
    	console.log(i);
        const lastSlash = window.location.pathname.lastIndexOf('/');
        const prevClusterName = window.location.pathname.substr(lastSlash + 1);
	$http.post('/api/clusters/' + i.label, {
		data: {
			label: i.label,
			oldLabel: prevClusterName,
			selectedImages: [i.image],
		}
	}).then(function (response) {
		if (response.status === 200) {
			vm.images = ClusterImagesService.query({
				label: $stateParams.label
			});
		}	
	});
    }

    $scope.changeAllLabels = function() {
        const lastSlash = window.location.pathname.lastIndexOf('/');
        const prevClusterName = window.location.pathname.substr(lastSlash + 1);
        $http.post('/api/clusters/' + vm.fullClusterName, {
          data: {
                label: vm.fullClusterName,
                oldLabel: prevClusterName,
		selectedImages: vm.selected,
          }
        }).then(function (response){
		console.log(response);
		if (response.status === 200) {
			vm.images = ClusterImagesService.query({
				label: $stateParams.label
			});
		}
	});
    }

    $scope.selectAll = function() {
   	vm.images.forEach(i => vm.selected.push(i.image)); 
    }

    $scope.deselectAll = function() {
    	vm.selected = []; 
    }

    $scope.selectImage = function(img) {
	const index = vm.selected.indexOf(img);
   	if (index >= 0) {
		vm.selected.splice(index, 1);
	} else {
		vm.selected.push(img);	
	}
    }

    $scope.currentPage = 0;
    $scope.pageSize = 45;
    $scope.numberOfPages= () => {
        return Math.ceil(vm.images.length / $scope.pageSize);
    }
  
  }
}());
