(function () {
  'use strict';

  angular
    .module('clusters')
    .controller('ClusterListController', ClusterListController);

  ClusterListController.$inject = ['ClustersService', '$scope'];

  function ClusterListController(ClustersService, $scope) {
    var vm = this;
    vm.cluster = ClustersService.query();
    /*
    $scope.filterItems = {
      'numDays': true
    };
    $scope.testFilter = function(browse) {
      console.log(browse._id);
      return $scope.filterItems[browse.numDays];
    };
    */
    console.log(vm.cluster);
  }
}());
