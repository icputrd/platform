(function () {
  'use strict';

  angular
    .module('clusters.services')
    .factory('ClustersService', ClustersService);

  ClustersService.$inject = ['$resource', '$log'];

  function ClustersService($resource, $log) {
    console.log('clusters service');
    var Clusters = $resource('/api/clusters', {
      id: '@_id',
      label: 'label'
    }, {
      update: {
        method: 'PUT'
      }
    });
    console.log('Clusters');
    return Clusters;
  }
}());
