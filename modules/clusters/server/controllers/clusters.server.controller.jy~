'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Cluster = mongoose.model('Clusters'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * List of Clusters
 */
exports.list = function (req, res) {
  Cluster.distinct('label').sort().exec(function (err, clusters) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(clusters);
    }
  });
};

/** *********************************************************************************************************/
/**
 * update a cluster
 */
exports.update = function (req, res) {
  console.log('update- ' + JSON.stringify(req.body));
  var cluster = req.cluster;
  cluster.cluster_name = req.body.cluster_name;
  cluster.image = req.body.image;
  cluster.location = req.body.location;
  cluster.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(cluster);
    }
  });
};
/**
 * Create a cluster
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));

  var data = req.body.data;
  var cluster = new Cluster(data);

  Cluster.find({ 'image': cluster.image }).exec(function (err, clusters) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      if (clusters.length !== 0) {
        clusters[0].cluster_name = cluster.cluster_name;
        clusters[0].label = cluster.label;
        cluster = clusters[0];
      }
      cluster.save(function (err) {
        if (err) {
          return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.json(cluster);
        }
      });
    }
  });
};

exports.move = function(req, res) {
	const oldLabel = req.body.data.oldLabel;
	Cluster.find({'label': oldLabel}).exec(function (err, clusters) {
		if (err) {
		    return res.status(422).send({
			message: errorHandler.getErrorMessage(err)
		    });
		} else {
			for (let i = 0; i < clusters.length; i++){
				clusters[i].label = req.body.data.label;
				clusters[i].save(function(err) {
					if (err) {
					    return res.status(422).send({
						message: errorHandler.getErrorMessage(err)
					    });
					}
				});
			}
		}
	});
	res.json({'status': 'done'});
};

/**
 * Show the current cluster
 */
exports.read = function (req, res) {
  var cluster = req.cluster ? JSON.stringify(req.body) : {};
  cluster = req.cluster;
  res.json(cluster);
};

/**
 * Cluster middleware
 */
exports.clusterByID = function (req, res, next, id) {
  console.log('clusterByID- ' + JSON.stringify(req.body));
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Cluster is invalid'
    });
  }

  Cluster.findById(id).exec(function (err, cluster) {
    if (err) {
      return next(err);
    } else if (!cluster) {
      return res.status(404).send({
        message: 'No cluster with that identifier has been found'
      });
    }
    req.cluster = cluster;
    next();
  });
};

/**
 * Label middleware
 */
exports.label = function (req, res, next, label) {
  console.log('label- ' + JSON.stringify(req.body));
  Cluster.find({ label: label }).exec(function (err, cluster) {
    if (err) {
      return next(err);
    } else if (!cluster) {
      return res.status(404).send({
        message: 'No cluster with that label has been found'
      });
    }
    req.cluster = cluster;
    next();
  });
};

