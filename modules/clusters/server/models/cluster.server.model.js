'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Cluster Schema
 */
var ClusterSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  cluster_name: {
    type: String,
    default: '',
    trim: true,
    required: 'Cluster name cannot be blank'
  },
  label: {
    type: String,
    default: ''
  },
  image: {
    type: String,
    default: ''
  }
});


var createCompoundIndex = function(db, callback) {
//  Get the documents collection
  var collection = db.collection('clusters');
  // Create the index
  collection.createIndex(
    { '$**': 'text' }, function(err, result) {
      console.log(result);
      callback(result);
    });
};
mongoose.model('Clusters', ClusterSchema);
