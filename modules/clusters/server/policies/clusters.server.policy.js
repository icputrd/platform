'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Cluster Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/clusters',
      permissions: '*'
    }, {
      resources: '/api/clusters/create',
      permissions: '*'
    }, {
      resources: '/api/clusters/:clusterId',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/clusters',
      permissions: ['*']
    }, {
      resources: '/api/clusters/image/:photo',
      permissions: '*'
    }, {
      resources: '/api/clusters/:clusterId',
      permissions: ['*']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/clusters',
      permissions: ['*']
    }, {
      resources: '/api/clusters/image/:photo',
      permissions: '*'
    }, {
      resources: '/api/clusters/:clusterId',
      permissions: ['*']
    }]
  }]);
};

/**
 * Check If Cluseter Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];
  console.log(req.user);

  // If an cluster is being processed and the current user created it then allow any manipulation
  if (req.cluster && req.user) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
