﻿(function () {
  'use strict';

  // Configuring the Demography Admin module
  angular
    .module('demography.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Demography',
      state: 'admin.demography.list'
    });
  }
}());
