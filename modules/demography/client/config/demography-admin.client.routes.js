﻿(function () {
  'use strict';

  angular
    .module('demography.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.demography', {
        abstract: true,
        url: '/demography',
        template: '<ui-view/>'
      })
      .state('admin.demography.list', {
        url: '',
        templateUrl: '/modules/demography/client/views/admin/list-demography.client.view.html',
        controller: 'DemographyAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.demography.create', {
        url: '/create',
        templateUrl: '/modules/demography/client/views/admin/form-demography.client.view.html',
        controller: 'DemographyAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          demographyResolve: newDemography
        }
      })
      .state('admin.demography.edit', {
        url: '/:demographyId/edit',
        templateUrl: '/modules/demography/client/views/admin/form-demography.client.view.html',
        controller: 'DemographyAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          demographyResolve: getDemography
        }
      });
  }

  getDemography.$inject = ['$stateParams', 'DemographyService'];

  function getDemography($stateParams, DemographyService) {
    return DemographyService.get({
      demographyId: $stateParams.demographyId
    }).$promise;
  }

  newDemography.$inject = ['DemographyService'];

  function newDemography(DemographyService) {
    return new DemographyService();
  }
}());
