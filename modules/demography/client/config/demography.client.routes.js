(function () {
  'use strict';

  angular
    .module('demography.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('demography', {
        abstract: true,
        url: '/demography',
        template: '<ui-view/>'
      })
      .state('demography.search', {
        url: '/search',
        templateUrl: '/modules/demography/client/views/search-demography.client.view.html',
        controller: 'DemographySearchController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Demography Search'
        }
      })
      .state('demography.list', {
        url: '',
        templateUrl: '/modules/demography/client/views/list-demography.client.view.html',
        controller: 'DemographyListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Demography List'
        }
      });
  }

  getDemography.$inject = ['$stateParams', 'DemographyService'];

  function getDemography($stateParams, DemographyService) {
    return DemographyService.get({
      demographyId: $stateParams.demographyId
    }).$promise;
  }
// //////////////////////////////
  getTag.$inject = ['$stateParams', 'TagsService'];

  function getTag($stateParams, TagsService) {
    return TagsService.get({
      TagId: $stateParams.TagId
    }).$promise;
  }
}());
