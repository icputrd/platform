﻿(function () {
  'use strict';

  angular
    .module('demography.admin')
    .controller('DemographyAdminController', DemographyAdminController);

  DemographyAdminController.$inject = ['$scope', '$state', '$window', 'demographyResolve', 'Authentication', 'Notification'];

  function DemographyAdminController($scope, $state, $window, demography, Authentication, Notification) {
    var vm = this;

    vm.demography = demography;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Demography
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.demography.$remove(function() {
          $state.go('admin.demography.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Demography deleted successfully!' });
        });
      }
    }

    // Save Demography
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.demographyForm');
        return false;
      }

      // Create a new demography, or update the current instance
      vm.demography.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.demography.list'); // should we send the User to the list or the updated Demography's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Demography saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Demography save error!' });
      }
    }
  }
}());
