﻿(function () {
  'use strict';

  angular
    .module('demography.admin')
    .controller('DemographyAdminListController', DemographyAdminListController);

  DemographyAdminListController.$inject = ['DemographyService'];

  function DemographyAdminListController(DemographyService) {
    var vm = this;

    vm.demography = DemographyService.query();
  }
}());
