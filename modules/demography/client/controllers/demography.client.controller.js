(function () {
  'use strict';

  angular
    .module('demography')
    .controller('DemographyController', DemographyController);

  DemographyController.$inject = ['$scope', 'demographyResolve', 'Authentication'];

  function DemographyController($scope, demography, Authentication) {
    var vm = this;

    vm.demography = demography;
    vm.authentication = Authentication;

  }
}());
