(function () {
  'use strict';

  angular
    .module('images')
    .controller('DemographyListController', DemographyListController);

  DemographyListController.$inject = ['DemographyService', '$scope', '$http', 'demographyFactory'];

  function DemographyListController(DemographyService, $scope, $http, demographyFactory) {
    var vm = this;
    var url = 'api/demography';
    $scope.getStuff = function() {
      $http.get(url).then(function(response) {
        vm.demo = response.data;
        console.log(response.data);
      });
    };
  }
}());
