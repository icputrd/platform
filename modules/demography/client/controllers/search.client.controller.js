(function () {
  'use strict';

  angular
    .module('demography')
//    .factory('openweathermapFactory', openweathermapFactory)
    .controller('DemographySearchController', DemographySearchController);
    // $q = new Promise;
  DemographySearchController.$inject = ['$q', '$scope', '$http', 'demographyFactory', 'DemographyService', '$location', 'dimageFactory', 'donorClassFactory', 'TagsService', '$log', 'openweathermapFactory'];

  function DemographySearchController($q, $scope, $http, demographyFactory, DemographyService, $location, dimageFactory, donorClassFactory, ImagesService, TagsService, $log, openweathermapFactory) {
    var vm = this;
    var url = 'http://localhost:3000/api/demography/';
    var ht;
    var objCount = {};
    var objCountArray = [];
    var counter = 0;
    var TagsFromServer;
    var PhotosFromServer;
    var PFS = 'utid';
    var _appid = 'e85ddf1df47e521f3dce3d52b642caca';
// ========================s============================
// Below:
// Logic for
// Wants array getting object
 //   var tags = DemographyService.query();
    $scope.$watch('searchDemo', function(newV, oldV, scope) {
      if (counter > 0) {
        demographyFactory.searchByField($scope.searchDemo)
          .then(function(response) {
            ht = angular.fromJson(response.data);
          });
      }
      counter = 100;
    });
    // Load all data to create files for searching
    $scope.$on('$viewContentLoaded', function() {

      var promiseA = donorClassFactory.getAllTags('');
      var promiseB = demographyFactory.searchByField(PFS);
//      donorClassFactory.angularOpenWeather();
      var PromiseC = promiseA.then(function(result1) {
        promiseB.then(function(result2) {
          donorClassFactory.countImagesPerDonor(result1, result2);
        });
      });
    });
    donorClassFactory.angularOpenWeather();
    $scope.submit = function(data) {
      console.log(data);
    };
    $scope.reset = function(form) {
      $scope.user = {
        sex: null,
        ancestry: null,
        ageAtDeath: null,
        startDate: null,
        endDate: null,
        weight: null
      };
      form.$setPristine();
      form.$setUntouched();
      form.$valid = true;
      form.$invalid = false;
      form.$error = {};
    };

    $scope.change = function() {
      $scope.city = 'Knoxville';
      $scope.units = 'metric';
      // Fetch the data from the public API through JSONP.
      // See http://openweathermap.org/API#weather.
      var url = 'http://api.openweathermap.org/data/2.5/weather?id=524901&APPID=' + _appid;
      var promiseA = $http.jsonp(url, { params: {
        q: $scope.city,
        units: $scope.units,
        callback: 'JSON_CALLBACK'
      } });
      var promiseB = promiseA.success(function(demo) {
        console.log(demo);
      });

    };
    $scope.change2 = function() {
      var proA = donorClassFactory.myWeather();
      var proB = proA.success(function(result) {
        console.log(result);
      });
    };
    var retValUT = null;
// Search by UTID
    $scope.searchUTID = function() {
      if ($scope.utid == null) {
        $scope.value = 'Not a valid UTID, Please Try Again';
        return;
      }
      $scope.utid = $scope.utid.toUpperCase();
      demographyFactory.getUTID($scope.utid)
        .then(function(response) {
          retValUT = angular.fromJson(response.data);
        });
      if (retValUT == null) {
        $scope.value = 'Not a valid UTID, Please Try Again';
      }
      vm.items = null;
      $scope.value = retValUT;
    };
    $scope.firstSort = function() {
      if ($scope.searchString == null) $scope.searchString = 'nil';
      $scope.value = null;
      vm.tags = ht;
      vm.dummyItems = ht; // dummy array of items to be paged
      vm.pager = {};
      vm.setPage = setPage;

      initController();

      function initController() {
          // initialize to page 1
        vm.setPage(1);
      }

      function setPage(page) {
        if (page < 1 || page > vm.pager.totalPages) {
          return;
        }
        vm.pager = demographyFactory.pageService(vm.dummyItems.length, page, 10);
          // get current page of items
        vm.items = vm.dummyItems.slice(vm.pager.getPager.startIndex, vm.pager.getPager.endIndex + 1);

      }
    };
    $scope.secondSort = function(demo) {
      if ($scope.searchString == null) $scope.searchString = 'nil';
      $scope.value = null;
      vm.tags = null;
      vm.dummyItems2 = demo; // dummy array of items to be paged
      vm.pager2 = {};
      vm.setPage2 = setPage2;
      vm.items2 = null;

      initController();

      function initController() {
          // initialize to page 1
        vm.setPage2(1);
      }

      function setPage2(page2) {
        if (page2 < 1 || page2 > vm.pager2.totalPages) {
          return;
        }
        vm.pager2 = demographyFactory.pageService(vm.dummyItems2.data.length, page2, 10);
          // get current page of items
        vm.items2 = vm.dummyItems2.data.slice(vm.pager2.getPager.startIndex, vm.pager2.getPager.endIndex + 1);

      }
    };

// =======================================
    $scope.analyzeResponse = function(demo) {
      var promiseA = dimageFactory.searchUT(demo.utid);
      var PromiseB = promiseA.then(function(response) {
        $scope.secondSort(response);
      });
    };
    $scope.goToLink = function(demo) {
      var promiseA = dimageFactory.searchUT(demo.utid);
      var PromiseB = promiseA.then(function(response) {
        $scope.secondSort(response);
      });
    };
  }
}());
