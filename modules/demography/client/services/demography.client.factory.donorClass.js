(function () {
  'use strict';

  angular
    .module('demography.services')
    .factory('donorClassFactory', donorClassFactory);

  donorClassFactory.$inject = ['$resource', '$log', '$http', 'demographyFactory', 'TagsService', 'openweathermapFactory'];

  function donorClassFactory($resource, $log, $http, demographyFactory, TagsService, openweathermapFactory) {

    function countImagesPerDonor(tagInfo, donorInfo) {
      var goodDonorInfo = angular.fromJson(donorInfo.data);
      var goodTagInfo = angular.fromJson(tagInfo.data);
      return 'to me';
    }
    function tagsPerImage() {

    }
    function getAllTags(id) {
      var pop = 'http://localhost:3000/api/tags';
      var ural = pop;
      console.log(ural);
      var sp = TagsService.query();
      return $http.get(pop)
        .then(function(response) {
          return response;
        });
    }
// Extra api calls for openweathermap
// "London,uk" // lang: '<LANGUAGE>', // units: '<UNITS>', // type: '<TYPE>', // units: $scope.units,
    function angularOpenWeather() {
      var _appid = 'e85ddf1df47e521f3dce3d52b642caca';
      openweathermapFactory.getWeatherFromCitySearchByName({
        q: 'Knoxville,us',
        appid: _appid
      }).then(function(_data) {
        console.log(_data);
        return _data;
      // on success
      }).catch(function (_data) {
        console.log(_data);
      // on error
      });
    }

    function myWeather() {
//      var city = 'Knoxville';
//      var units = 'metric';
      var _appid = 'e85ddf1df47e521f3dce3d52b642caca';
      // Fetch the data from the public API through JSONP.
      // See http://openweathermap.org/API#weather.
      var url = 'http://api.openweathermap.org/data/2.5/weather?id=524901&APPID=' + _appid;
//      return "I will go to the publix at 9:00 to see if short hair is there";
      return $http.jsonp(url, { params: {
        q: 'Knoxville',
        units: 'metric',
        callback: 'JSON_CALLBACK'
      } });
    }

    // // // // // // // // // // // // // // // // // // // // // // // //
    return {
      angularOpenWeather: angularOpenWeather,
      myWeather: myWeather,
      getAllTags: getAllTags,
      tagsPerImage: tagsPerImage,
      countImagesPerDonor: countImagesPerDonor
    };
  }
}());
