(function () {
  'use strict';

  angular
    .module('demography.services')
    .factory('demographyFactory', demographyFactory);

  demographyFactory.$inject = ['$resource', '$log', '$http'];

  function demographyFactory($resource, $log, $http) {
    var pop = 'http://localhost:3000/api/demography/byut/';
    var url = 'http://localhost:3000/api/demography/';
    function add() {
    }
    function getList() {
      return $http.get(url)
      .then(function(response) {
        return response.data;
      });
    }
    function getUTID(id) {
      var ural = pop + id;
      return $http.get(ural)
      .then(function(response) {
        return response;
      });
    }
    function searchByField(birl) {
      var ural = url + 'orgo/' + birl;
      return $http.get(ural)
      .then(function(response) {
        return response;
      });
    }
    function pageService(totalItems, currentPage, pageSize) {

      var service = {};

      service.getPager = getPager(totalItems, currentPage, pageSize);
      return service;


      function getPager(totalItems, currentPage, pageSize) {

        currentPage = currentPage || 1;

        pageSize = pageSize || 10;


        var totalPages = Math.ceil(totalItems / pageSize);

        var startPage;
        var endPage;
        if (totalPages <= 10) {
          startPage = 1;
          endPage = totalPages;
        } else {
          if (currentPage <= 6) {
            startPage = 1;
            endPage = 10;
          } else if (currentPage + 4 >= totalPages) {
            startPage = totalPages - 9;
            endPage = totalPages;
          } else {
            startPage = currentPage - 5;
            endPage = currentPage + 4;
          }
        }

        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
        var len = endPage - startPage;
        var val = startPage;
        var pages = Array.from({ length: len + 1 }, (v, i) => i);
        pages = Array.from(pages, x => x + val);
        return {
          totalItems: totalItems,
          currentPage: currentPage,
          pageSize: pageSize,
          totalPages: totalPages,
          startPage: startPage,
          endPage: endPage,
          startIndex: startIndex,
          endIndex: endIndex,
          pages: pages
        };
      }
    }
    return {
      add: add,
      searchByField: searchByField,
      getUTID: getUTID,
      list: getList,
//      getPager: getPager,
      pageService: pageService
    };
  }
}());

