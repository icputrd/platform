'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Demography = mongoose.model('Demography'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an demography
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));
  var demography = new Demography(req.body);
  demography.user = req.user;
  demography.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(demography);
    }
  });
};

/**
 * Show the current demography
 */
exports.read = function (req, res) {
  console.log('read - ' + JSON.stringify(req.body));
  var demography = req.demography ? req.demography.toJSON() : {};
  // Add a custom field to the Demography, for determining if the current User is the "owner".
  demography.isCurrentUserOwner = !!(req.user && demography.user && demography.user._id.toString() === req.user._id.toString());
  res.json(demography);
};
/**
 * Update an demography
 */
exports.update = function (req, res) {
  console.log('update- ' + JSON.stringify(req.body));
  var demography = req.demography;
  demography.demography = req.body.demography;
  demography.image = req.body.image;
  demography.location = req.body.location;
  demography.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(demography);
    }
  });
};

/**
 * Delete an demography
 */
exports.delete = function (req, res) {
  var demography = req.demography;
  demography.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(demography);
    }
  });
};

/**
 * List of Demographys
 */
exports.list = function (req, res) {
  console.log('list- ' + req);
  Demography.find().sort('-ageatdeath').populate('user', 'displayName').exec(function (err, demographys) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(demographys);
    }
  });
};
/**
 * Demography middleware
 */
exports.demographyByID = function (req, res, next, id) {
  console.log('demographyByID in demography.server.controller- ' + JSON.stringify(req.body));
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Demography is invalid'
    });
  }

  Demography.findById(id).populate('user', 'displayName').exec(function (err, demography) {
    if (err) {
      return next(err);
    } else if (!demography) {
      return res.status(404).send({
        message: 'No demography with that identifier has been found'
      });
    }
    req.demography = demography;
    next();
  });
};

// using app.param to deconstruct url to get image for search.
exports.imageID = function (req, res, next, id) {
  // console.log('demographyByID ugh ugh- ' + JSON.stringify(req.body));
  var plus = 'http://localhost:3000/';
  plus = plus + id;
  Demography.find({ 'image': plus }).populate('user', 'displayName').exec(function (err, demography) {
    if (err) {
      return next(err);
    } else if (!demography) {
      return res.status(404).send({
        message: 'No demography with that identifier has been found'
      });
    }
    req.demography = demography;
    next();
  });
};
// Copy exports.read
exports.readImage = function (req, res) {
 // console.log('read - ' + JSON.stringify(req.body));
  var demography = req.demography;
  res.json(demography);
};
//  Return String by UTID
exports.ByUT = function (req, res, next, id) {
  Demography.find({ 'utid': id }).sort(id).exec(function (err, demography) {
    if (err) {
      return next(err);
    } else if (!demography) {
      return res.status(404).send({
        message: 'No demography with that identifier has been found'
      });
    }
    req.demography = demography;
    next();
  });
};

exports.readUT = function (req, res) {
  var demography = req.demography ? JSON.stringify(req.demography) : {};

  // Add a custom field to the Demography, for determining if the current User is the "owner".
  res.json(demography);
};
// ORGANIZE by FIELD
exports.orgoByID = function (req, res, next, id) {
  Demography.find().sort(id).exec(function (err, demography) {
    if (err) {
      return next(err);
    } else if (!demography) {
      return res.status(404).send({
        message: 'No demography with that identifier has been found'
      });
    }
    req.demography = demography;
    next();
  });
};

exports.readOrgo = function (req, res) {
  var demography = req.demography ? JSON.stringify(req.demography) : {};
  // Add a custom field to the Demography, for determining if the current User is the "owner".
  res.json(demography);
};

