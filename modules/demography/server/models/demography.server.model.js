'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Demography Schema
 */
var DemographySchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  utid: {
    type: String,
    default: '',
    index: true,
    required: 'UTID must be unique'
  },
  year: {
    type: Date,
    default: ''
  },
  sex: {
    type: String,
    default: ''
  },
  ageatdeath: {
    type: Number,
    default: ''
  },
  ancestry: {
    type: String,
    default: ''
  },
  stature: {
    type: Number,
    default: ''
  },
  weight: {
    type: Number,
    default: ''
  },
  dateplaced: {
    type: Date,
    default: ''
  }
});
DemographySchema.index({ '$**': 'text' });
mongoose.model('Demography', DemographySchema, 'demography');
