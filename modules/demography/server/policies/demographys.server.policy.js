'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Demographys Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/demography',
      permissions: '*'
    }, {
      resources: '/api/demography/create',
      permissions: '*'
    }, {
      resources: '/api/demography/:demographyId',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/demography',
      permissions: ['*']
    }, {
      resources: '/api/demography/image/:demod',
      permissions: '*'
    }, {
      resources: '/api/demography/orgo/:searchDemo',
      permissions: '*'
    }, {
      resources: '/api/demography/byut/:utSingleSearch',
      permissions: '*'
    }, {
      resources: '/api/demography/:demographyId',
      permissions: ['*']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/demography',
      permissions: ['*']
    }, {
      resources: '/api/demography/image/:demod',
      permissions: '*'
    }, {
      resources: '/api/demography/orgo/:searchDemo',
      permissions: '*'
    }, {
      resources: '/api/demography/byut/:utSingleSearch',
      permissions: '*'
    }, {
      resources: '/api/demography/:demographyId',
      permissions: ['*']
    }]
  }]);
};

/**
 * Check If Demographys Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an demography is being processed and the current user created it then allow any manipulation
  if (req.demography && req.user && req.demography.user && req.demography.user.id === req.user.id) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
