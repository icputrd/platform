'use strict';

/**
 * Module dependencies
 */
var demographyPolicy = require('../policies/demographys.server.policy'),
  demography = require('../controllers/demographys.server.controller');

module.exports = function (app) {
  // Single by UTID
  app.route('/api/demography/byut/:utSingleSearch')
    .all(demographyPolicy.isAllowed)
    .get(demography.readUT);
  // Finish by binding the demography middleware
  app.param('utSingleSearch', demography.ByUT);
  // List organized by Demograhpy Characteristic
  app.route('/api/demography/orgo/:searchDemo')
    .all(demographyPolicy.isAllowed)
    .get(demography.readOrgo);
  // Finish by binding the demography middleware
  app.param('searchDemo', demography.orgoByID);
  // Routes not needing a Demography ID
  app.route('/api/demography')
    .all(demographyPolicy.isAllowed)
    .get(demography.list)
    .post(demography.create);
  // Routes needing a Demograhpy ID
  app.route('/api/demography/:demographyId')
    .all(demographyPolicy.isAllowed)
    .get(demography.read)
    .put(demography.update)
    .delete(demography.delete);
  // Finish by binding the demography middleware
  app.param('demographyId', demography.demographyByID);
  // New route for image only demography
  app.route('/api/demography/image/:imageId')
    .all(demographyPolicy.isAllowed)
    .get(demography.readImage);
  app.param('imageId', demography.imageID);
};
