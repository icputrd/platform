﻿(function () {
  'use strict';

  // Configuring the Dimages Admin module
  angular
    .module('dimages.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Dimages',
      state: 'admin.dimages.list'
    });
  }
}());
