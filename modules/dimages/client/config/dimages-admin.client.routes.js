﻿(function () {
  'use strict';

  angular
    .module('dimages.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.dimages', {
        abstract: true,
        url: '/dimages',
        template: '<ui-view/>'
      })
      .state('admin.dimages.list', {
        url: '',
        templateUrl: '/modules/dimages/client/views/admin/list-dimages.client.view.html',
        controller: 'DimagesAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.dimages.create', {
        url: '/create',
        templateUrl: '/modules/dimages/client/views/admin/form-dimage.client.view.html',
        controller: 'DimagesAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          dimageResolve: newDimage
        }
      })
      .state('admin.dimages.edit', {
        url: '/:dimageId/edit',
        templateUrl: '/modules/dimages/client/views/admin/form-dimage.client.view.html',
        controller: 'DimagesAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          dimageResolve: getDimage
        }
      });
  }

  getDimage.$inject = ['$stateParams', 'DimagesService'];

  function getDimage($stateParams, DimagesService) {
    return DimagesService.get({
      dimageId: $stateParams.dimageId
    }).$promise;
  }

  newDimage.$inject = ['DimagesService'];

  function newDimage(DimagesService) {
    return new DimagesService();
  }
}());
