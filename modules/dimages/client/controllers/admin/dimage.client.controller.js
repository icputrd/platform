﻿(function () {
  'use strict';

  angular
    .module('dimages.admin')
    .controller('DimagesAdminController', DimagesAdminController);

  DimagesAdminController.$inject = ['$scope', '$state', '$window', 'dimageResolve', 'Authentication', 'Notification'];

  function DimagesAdminController($scope, $state, $window, dimage, Authentication, Notification) {
    var vm = this;

    vm.dimage = dimage;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Dimage
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.dimage.$remove(function() {
          $state.go('admin.dimages.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Dimage deleted successfully!' });
        });
      }
    }

    // Save Dimage
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.dimageForm');
        return false;
      }

      // Create a new dimage, or update the current instance
      vm.dimage.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.dimages.list'); // should we send the User to the list or the updated Dimage's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Dimage saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Dimage save error!' });
      }
    }
  }
}());
