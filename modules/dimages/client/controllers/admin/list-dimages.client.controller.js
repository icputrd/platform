﻿(function () {
  'use strict';

  angular
    .module('dimages.admin')
    .controller('DimagesAdminListController', DimagesAdminListController);

  DimagesAdminListController.$inject = ['DimagesService'];

  function DimagesAdminListController(DimagesService) {
    var vm = this;

    vm.dimages = DimagesService.query();
  }
}());
