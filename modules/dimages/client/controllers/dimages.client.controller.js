(function () {
  'use strict';

  angular
    .module('dimages')
    .controller('DimagesController', DimagesController);

  DimagesController.$inject = ['$scope', 'dimageResolve', 'Authentication'];

  function DimagesController($scope, dimage, Authentication) {
    var vm = this;

    vm.dimage = dimage;
    vm.authentication = Authentication;

  }
}());
