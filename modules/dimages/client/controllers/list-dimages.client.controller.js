(function () {
  'use strict';

  angular
    .module('dimages')
    .controller('DimagesListController', DimagesListController);

  DimagesListController.$inject = ['$scope', 'Authentication', 'DimagesService',
    'dimageFactory'];

  function DimagesListController($scope, Authentication, DimagesService,
    dimageFactory) {
    $scope.searchString = '';
    var utidHttp;
    var vm = this;
    var rd = null; // response data
    var id = dimageFactory.get();
    console.log(id);
    $scope.utidInfo = id;
// ======================================================
// getting selected UTID from server
    dimageFactory.searchUT(id.utid)
      .then(function(response) {
        rd = response.data;
        console.log('utidHttp', rd);
        $scope.numImages = rd.length;
      });
    dimageFactory.deconstruct(rd);
// ======================================================
    $scope.showDimages = function() {
      vm.utidHttp = rd;
    };
    $scope.clear = function() {
      $scope.searchString = '';
    };
  }
}());
