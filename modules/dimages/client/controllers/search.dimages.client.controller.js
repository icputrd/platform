(function () {
  'use strict';

  angular
    .module('dimages')
    .controller('SearchDimagesController', SearchDimagesController);

  SearchDimagesController.$inject = ['$scope', 'Authentication', 'DimagesService',
    'dimageFactory'];

  function SearchDimagesController($scope, Authentication, DimagesService,
    dimageFactory) {
    var vm = this;
    vm.authentication = Authentication;
    $scope.searchString = '';
    var ageHttp;
    var raceHttp;
    var daysHttp;
// ======================================================
    $scope.search = function() {
      if (!$scope.searchString) $scope.searchString = 'nil';
    //  console.log(dimageFactory.searchAge());
/*      dimageFactory.searchAncestry('white')
        .then(function(response) {
          raceHttp = response.data;
          console.log('raceHttp', raceHttp);
        });
*/
      dimageFactory.searchDays('30')
        .then(function(response) {
          daysHttp = response.data;
          console.log('daysHttp', daysHttp);
        });
      dimageFactory.searchAge('30')
        .then(function(response) {
          ageHttp = response.data;
          console.log('ageHttp', ageHttp);
        });
    };
// ======================================================
    $scope.clear = function() {
      $scope.searchString = '';
    };
  }
}());
