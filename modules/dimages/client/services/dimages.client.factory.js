(function () {
  'use strict';

  angular
    .module('dimages.services')
    .factory('dimageFactory', dimageFactory);

  dimageFactory.$inject = ['$resource', '$log', '$http'];

  function dimageFactory($resource, $log, $http) {
    var url = 'http://localhost:3000/api/dimages/UT01-15D';
    var urlage = 'http://localhost:3000/api/dimages/age/';
    var urldays = 'http://localhost:3000/api/dimages/days/';
    var urlancestry = 'http://localhost:3000/api/dimages/ancestry/';
    var pop = 'http://localhost:3000/api/dimages/search/';
    var urlUTID = 'http://localhost:3000/api/dimages/utid/';
    var getSet = null;
// ==============================================
    function searchRace(gen) {
      gen = 'white';
      var rl = urlancestry + gen;
      console.log('searchStature');
      return $http.get(rl)
        .then(function(response) {
          console.log(response);
          return response;
        });
    }
// ==============================================
    function searchDays(age) {
      age = '30';
      var rl = urldays + age;
      console.log('searchDays');
      return $http.get(rl)
        .then(function(response) {
          console.log(response);
          return response;
        });
    }
// ==============================================
    function searchUT(utid) {
      var rl = urlUTID + utid;
      console.log('searchUT', rl);
      return $http.get(rl)
        .then(function(response) {
          console.log(response);
          return response;
        });
    }
// ==============================================
    function searchAge(age) {
      age = '30';
      var rl = urlage + age;
      console.log('searchAge');
      return $http.get(rl)
        .then(function(response) {
          console.log(response);
          return response;
        });
    }
// ==============================================
    function find(ow) {
      var rl = pop + ow;
      return $http.get(url)
        .then(function(response) {
          console.log(response);
          return response;
        });
    }
    function add() {
      console.log('add here');
      $http.get(url).then(function(response) {
        return response;
      });
    }
    function deconstruct(word) {
      console.log('fact decon', word);
      return word;
    }

    function set(data) {
      getSet = data;
    }
    var get = function () {
      return getSet;
    };
    var wait = function() {
      deconstruct(function () {
      });
      return;
    };
    return {
      add: add,
      searchAge: searchAge,
      searchDays: searchDays,
      searchAncestry: searchRace,
      searchUT: searchUT,
      get: get,
      set: set,
      find: find,
      deconstruct: deconstruct
    };

  }
}());
