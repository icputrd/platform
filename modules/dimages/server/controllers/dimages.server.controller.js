'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Dimage = mongoose.model('Dimage'),
  querystring = require('querystring'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an dimage
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));
  var dimage = new Dimage(req.body);
  dimage.user = req.user;
  dimage.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(dimage);
    }
  });
};

/**
 * Show the current dimage
 */
exports.read = function (req, res) {
  console.log('read something else - ' + JSON.stringify(req.body), req.dimage);
  var dimage = req.dimage ? JSON.stringify(req.dimage) : {};
  // Add a custom field to the Dimage, for determining if the current User is the "owner".
//  dimage.isCurrentUserOwner = !!(req.user && dimage.user && dimage.user._id.toString() === req.user._id.toString());
  res.json(dimage);
};
/**
 * Update an dimage
 */
exports.update = function (req, res) {
  console.log('update- ' + JSON.stringify(req.body));
  var dimage = req.dimage;
  dimage.dimage = req.body.dimage;
  dimage.dimage = req.body.dimage;
  dimage.location = req.body.location;
  dimage.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(dimage);
    }
  });
};

/**
 * Delete an dimage
 */
exports.delete = function (req, res) {
  var dimage = req.dimage;
  dimage.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(dimage);
    }
  });
};

/**
 * List of Dimages
 */
exports.list = function (req, res) {
  console.log('list- ' + req);
  console.log('What is going on here');
  Dimage.find().exec(function (err, dimages) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(dimages);
    }
  });
};
/**
 * Dimage middleware
 */
exports.dimageByID = function (req, res, next, id) {
  console.log('What the? ');
  console.log(id);
  console.log('dimageByID- ' + JSON.stringify(req.body));

  Dimage.find({ 'utid': id }).exec(function (err, dimage) {
    if (err) {
      return next(err);
    } else if (!dimage) {
      return res.status(404).send({
        message: 'No dimage with that identifier has been found'
      });
    }
    req.dimage = dimage;
    next();
  });
};


exports.DimageID = function (req, res, next, id) {
  console.log('good function- ' + JSON.stringify(req.body));
  var plus = 'http://localhost:3000/';
  plus = plus + id;
  console.log('id here', id);
  Dimage.find({ 'demography.ageatdeath': id }).populate('user', 'displayName').exec(function (err, tag) {
    if (err) {
      return next(err);
    } else if (!tag) {
      return res.status(404).send({
        message: 'No tag with that identifier has been found'
      });
    }
    console.log(tag);
    req.tag = tag;
    next();
  });
};
// Copy exports.read
exports.readImage = function (req, res, id) {

  console.log('read first step- ' + req);
  var tag = req.tag;
  res.json(tag);
};

exports.findDaysMid = function (req, res, next, id) {
  console.log('find Days MID  - ' + JSON.stringify(req.body));
  var plus = 'http://localhost:3000/';
  plus = plus + id;
  console.log('id here', id);
  Dimage.find({ 'days': id }).populate('user', 'displayName').exec(function (err, tag) {
    if (err) {
      return next(err);
    } else if (!tag) {
      return res.status(404).send({
        message: 'No tag with that identifier has been found'
      });
    }
    req.tag = tag;
    next();
  });
};
// Copy exports.read
exports.findDays = function (req, res, id) {

  console.log('FIND Days- ' + req);
  var tag = req.tag;
  res.json(tag);
};

exports.findSexMid = function (req, res, next, id) {
  console.log('find Sex MID  - ' + JSON.stringify(req.body));
  console.log('id here', id);
  Dimage.find({ 'demography.ancestry': id }).populate('user', 'displayName').exec(function (err, tag) {
    if (err) {
      return next(err);
    } else if (!tag) {
      return res.status(404).send({
        message: 'No tag with that identifier has been found'
      });
    }
    req.tag = tag;
    next();
  });
};
// Copy exports.read
exports.findSex = function (req, res, id) {

  console.log('FIND SEX- ' + req);
  var tag = req.tag;
  res.json(tag);
};
// ===============================================
exports.findUTIDMid = function (req, res, next, id) {
  console.log('find utid MID  - ' + JSON.stringify(req.body));
  console.log('id here', id);
  Dimage.find({ 'utid': id }).populate('user', 'displayName').exec(function (err, tag) {
    if (err) {
      return next(err);
    } else if (!tag) {
      return res.status(404).send({
        message: 'No tag with that identifier has been found'
      });
    }
    req.tag = tag;
    next();
  });
};
// Copy exports.read
exports.findUTID = function (req, res, id) {

  console.log('FIND utid- ' + req);
  var tag = req.tag;
  res.json(tag);
};
