'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Dimage Schema
 */
var DimageSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  utid: {
    type: String,
    default: '',
    trim: true,
    index: true
  },
  url: {
    type: String,
    default: '',
    index: true,
    required: 'URL cannot be blank'
  },
  days: {
    type: Number,
    default: ''
  },
  dates: {
    type: Date,
    default: ''
  },
  pdates: {
    type: Date,
    default: ''
  },
  demography: [
    {
      utid: {
        type: String,
        default: '',
        trim: true,
        index: true
      },
      url: {
        type: Date,
        default: ''
      },
      sex: {
        type: String,
        default: ''
      },
      ageatdeath: {
        type: Number,
        default: ''
      },
      ancestry: {
        type: String,
        default: ''
      },
      stature: {
        type: Number,
        default: ''
      },
      weight: {
        type: Number,
        default: ''
      },
      dateplaced: {
        type: Date,
        default: ''
      }
    }
  ]
});

mongoose.model('Dimage', DimageSchema);
