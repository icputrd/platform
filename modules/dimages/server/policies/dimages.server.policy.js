'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Dimages Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/dimages',
      permissions: '*'
    }, {
      resources: '/api/dimages/create',
      permissions: '*'
    }, {
      resources: '/api/dimages/:dimageId',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/dimages',
      permissions: ['*']
    }, {
      resources: '/api/dimages/age/:age',
      permissions: '*'
    }, {
      resources: '/api/dimages/ancestry/:race',
      permissions: ['*']
    }, {
      resources: '/api/dimages/days/:days',
      permissions: '*'
    }, {
      resources: '/api/dimages/utid/:utid',
      permissions: '*'
    }, {
      resources: '/api/dimages/:dimageId',
      permissions: ['*']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/dimages',
      permissions: ['*']
    }, {
      resources: '/api/dimages/age/:age',
      permissions: ['*']
    }, {
      resources: '/api/dimages/days/:days',
      permissions: ['*']
    }, {
      resources: '/api/dimages/ancestry/:race',
      permissions: ['*']
    }, {
      resources: '/api/dimages/utid/:utid',
      permissions: '*'
    }, {
      resources: '/api/dimages/:dimageId',
      permissions: ['*']
    }]
  }]);
};

/**
 * Check If Dimages Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an dimage is being processed and the current user created it then allow any manipulation
  if (req.dimage && req.user && req.dimage.user && req.dimage.user.id === req.user.id) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
