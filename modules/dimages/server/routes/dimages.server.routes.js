'use strict';

/**
 * Module dependencies
 */
var dimagesPolicy = require('../policies/dimages.server.policy'),
  dimages = require('../controllers/dimages.server.controller');

module.exports = function (app) {
  // Dimages collection routes
  // New route for dimage only dimages
  app.route('/api/dimages/utid/:utid')
    .all(dimagesPolicy.isAllowed)
    .get(dimages.findUTID);
  app.param('utid', dimages.findUTIDMid);
// ==============================================
  app.route('/api/dimages/ancestry/:race')
    .all(dimagesPolicy.isAllowed)
    .get(dimages.findSex);
  app.param('race', dimages.findSexMid);
// ==============================================
  app.route('/api/dimages/days/:days')
    .all(dimagesPolicy.isAllowed)
    .get(dimages.findDays);
  app.param('days', dimages.findDaysMid);
// ==============================================
  app.route('/api/dimages/age/:age')
    .all(dimagesPolicy.isAllowed)
    .get(dimages.readImage);
// //////////////////////////////////////
  app.param('age', dimages.DimageID);
//  app.param('dimageId', dimages.dimageByID);
  app.route('/api/dimages')
    .all(dimagesPolicy.isAllowed)
    .get(dimages.list)
    .post(dimages.create);
  // Single dimage routes
  app.route('/api/dimages/:dimageId')
    .all(dimagesPolicy.isAllowed)
    .get(dimages.read)
    .put(dimages.update)
    .delete(dimages.delete);
  // Finish by binding the dimage middleware
  app.param('dimageId', dimages.dimageByID);
};
