﻿(function () {
  'use strict';

  // Configuring the Donors Admin module
  angular
    .module('donors.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Donors',
      state: 'admin.donors.list'
    });
  }
}());
