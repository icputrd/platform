(function () {
  'use strict';

  angular
    .module('donors.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('donors', {
        abstract: true,
        url: '/donors',
        template: '<ui-view/>'
      })
      .state('donors.list', {
        url: '',
        templateUrl: '/modules/donors/client/views/list-donors.client.view.html',
        controller: 'DonorsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Donors List'
        }
      });
  }

  getDonor.$inject = ['$stateParams', 'DonorsService'];

  function getDonor($stateParams, DonorsService) {
    return DonorsService.get({
      donorId: $stateParams.donorId
    }).$promise;
  }
}());
