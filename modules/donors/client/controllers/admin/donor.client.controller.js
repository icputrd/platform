﻿(function () {
  'use strict';

  angular
    .module('donors.admin')
    .controller('DonorsAdminController', DonorsAdminController);

  DonorsAdminController.$inject = ['$scope', '$state', '$window', 'donorResolve', 'Authentication', 'Notification'];

  function DonorsAdminController($scope, $state, $window, donor, Authentication, Notification) {
    var vm = this;

    vm.donor = donor;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Donor
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.donor.$remove(function() {
          $state.go('admin.donors.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Donor deleted successfully!' });
        });
      }
    }

    // Save Donor
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.donorForm');
        return false;
      }

      // Create a new donor, or update the current instance
      vm.donor.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.donors.list'); // should we send the User to the list or the updated Donor's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Donor saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Donor save error!' });
      }
    }
  }
}());
