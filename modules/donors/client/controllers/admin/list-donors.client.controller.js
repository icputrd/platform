﻿(function () {
  'use strict';

  angular
    .module('donors.admin')
    .controller('DonorsAdminListController', DonorsAdminListController);

  DonorsAdminListController.$inject = ['DonorsService'];

  function DonorsAdminListController(DonorsService) {
    var vm = this;

    vm.donors = DonorsService.query();
  }
}());
