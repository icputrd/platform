(function () {
  'use strict';

  angular
    .module('donors')
    .controller('DonorsListController', DonorsListController);

  DonorsListController.$inject = ['$scope', 'DonorsService', '$http', 'demographyFactory', 'tagFactory', 'imageFactory', 'utagFactory', 'dimageFactory'];

  function DonorsListController($scope, DonorsService, $http, demographyFactory, tagFactory, imageFactory, utagFactory, dimageFactory) {
    var vm = this;
    var result = [];
    var donorCount = 0;
    var photos = {};
// //////////////=== LIST ===////////////////////////
    $scope.list = function() {
      donorCount++;
      var url = 'http://localhost:3000/api/dimages';
      vm.images = $http.get(url);
      var demo = demographyFactory.add();
      var dimage = dimageFactory.add();
      var utag = utagFactory.add();
      console.log('demo', demo);
      console.log('image', dimage);
      console.log('tag', utag);
      console.log('vm.images', vm.images);
    };
  }
}());
