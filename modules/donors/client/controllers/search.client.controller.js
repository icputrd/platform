(function () {
  'use strict';

  angular
    .module('tags')
    .controller('SearchTagsController', SearchTagsController);

  SearchTagsController.$inject = ['$scope', 'Authentication'];

  function SearchTagsController($scope, Authentication) {
    var vm = this;

    vm.authentication = Authentication;

  }
}());
