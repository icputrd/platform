(function (app) {
  'use strict';

  app.registerModule('donors', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('donors.admin', ['core.admin']);
  app.registerModule('donors.admin.routes', ['core.admin.routes']);
  app.registerModule('donors.services');
  app.registerModule('donors.routes', ['ui.router', 'core.routes', 'donors.services']);
}(ApplicationConfiguration));
