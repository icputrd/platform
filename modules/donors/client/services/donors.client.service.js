(function () {
  'use strict';

  angular
    .module('donors.services')
    .factory('DonorsService', DonorsService);

  DonorsService.$inject = ['$resource', '$log'];

  function DonorsService($resource, $log) {
    var Donor = $resource('/api/donors/:donorId', {
      donorId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(Donor.prototype, {
      createOrUpdate: function () {
        var donor = this;
        return createOrUpdate(donor);
      }
    });

    return Donor;

    function createOrUpdate(donor) {
      if (donor._id) {
        return donor.$update(onSuccess, onError);
      } else {
        return donor.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(donor) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
