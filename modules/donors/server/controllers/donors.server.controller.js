'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Donor = mongoose.model('Donor'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an donor
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));
  console.log('donor.');
  var donor = new Donor(req.body);
  donor.user = req.user;
  donor.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(donor);
    }
  });
};

/**
 * Show the current donor
 */
exports.read = function (req, res) {
  console.log('read - ' + JSON.stringify(req.body));
  var donor = req.donor ? req.donor.toJSON() : {};
  // Add a custom field to the Donor, for determining if the current User is the "owner".
  donor.isCurrentUserOwner = !!(req.user && donor.user && donor.user._id.toString() === req.user._id.toString());
  res.json(donor);
};
/**
 * Update an donor
 */
exports.update = function (req, res) {
  console.log('update- ' + JSON.stringify(req.body));
  var donor = req.donor;
  donor.donor = req.body.donor;
  donor.image = req.body.image;
  donor.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(donor);
    }
  });
};

/**
 * Delete an donor
 */
exports.delete = function (req, res) {
  var donor = req.donor;
  donor.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(donor);
    }
  });
};

/**
 * List of Donors
 */
exports.list = function (req, res) {
  console.log('list- ' + req);
  Donor.find().sort('-created').populate('user', 'displayName').exec(function (err, donors) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(donors);
    }
  });
};
/**
 * Donor middleware
 */
exports.donorByID = function (req, res, next, id) {
  console.log('donorByID- ' + JSON.stringify(req.body));
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Donor is invalid'
    });
  }

  Donor.findById(id).populate('user', 'displayName').exec(function (err, donor) {
    if (err) {
      return next(err);
    } else if (!donor) {
      return res.status(404).send({
        message: 'No donor with that identifier has been found'
      });
    }
    req.donor = donor;
    next();
  });
};

