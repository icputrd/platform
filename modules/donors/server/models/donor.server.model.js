'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Donor Schema
 */
var DonorSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  utid: {
    type: String,
    default: ''
  },
  year: {
    type: String,
    default: ''
  },
  sex: {
    type: String,
    default: ''
  },
  ageatdeath: {
    type: String,
    default: ''
  },
  ancestry: {
    type: String,
    default: ''
  },
  stature_cm: {
    type: String,
    default: ''
  },
  weight: {
    type: String,
    default: ''
  },
  date_placed: {
    type: String,
    default: ''
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Donor', DonorSchema);
