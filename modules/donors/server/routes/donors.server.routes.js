'use strict';

/**
 * Module dependencies
 */
var donorsPolicy = require('../policies/donors.server.policy'),
  donors = require('../controllers/donors.server.controller');

module.exports = function (app) {
  // Donors collection routes
  app.route('/api/donors').all(donorsPolicy.isAllowed).get(donors.list).post(donors.create);
  // Single donor routes
  app.route('/api/donors/:donorId').all(donorsPolicy.isAllowed).get(donors.read).put(donors.update).delete(donors.delete);
  // Finish by binding the donor middleware
  app.param('donorId', donors.donorByID);
  // New route for image only donors
//  app.route('/api/donors/image/:imageId').all(donorsPolicy.isAllowed).get(donors.readImage);
//  app.param('imageId', donors.imageID);
};
