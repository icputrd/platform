﻿(function () {
  'use strict';

  // Configuring the Images Admin module
  angular
    .module('images.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Images',
      state: 'admin.images.list'
    });
  }
}());
