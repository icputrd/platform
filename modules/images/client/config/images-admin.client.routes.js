﻿(function () {
  'use strict';

  angular
    .module('images.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.images', {
        abstract: true,
        url: '/images',
        template: '<ui-view/>'
      })
      .state('admin.images.list', {
        url: '',
        templateUrl: '/modules/images/client/views/admin/list-images.client.view.html',
        controller: 'ImagesAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.images.create', {
        url: '/create',
        templateUrl: '/modules/images/client/views/admin/form-image.client.view.html',
        controller: 'ImagesAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          imageResolve: newImage
        }
      })
      .state('admin.images.edit', {
        url: '/:imageId/edit',
        templateUrl: '/modules/images/client/views/admin/form-image.client.view.html',
        controller: 'ImagesAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          imageResolve: getImage
        }
      });
  }

  getImage.$inject = ['$stateParams', 'ImagesService'];

  function getImage($stateParams, ImagesService) {
    return ImagesService.get({
      imageId: $stateParams.imageId
    }).$promise;
  }

  newImage.$inject = ['ImagesService'];

  function newImage(ImagesService) {
    return new ImagesService();
  }
}());
