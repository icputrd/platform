/*
(function () {
  'use strict';

  angular
    .module('images.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('images', {
        abstract: true,
        url: '/images',
        template: '<ui-view/>'
      })
      .state('images.list', {
        url: '',
        templateUrl: '/modules/images/client/views/list-images.client.view.html',
        controller: 'ImagesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Images List'
        }
      })
      .state('images.search', {
        url: '/search',
        templateUrl: '/modules/images/client/views/search-images.client.view.html',
        controller: 'SearchImagesController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Images Search'
        }
      })
      .state('images.view', {
        url: '/:imageId',
        templateUrl: '/modules/images/client/views/view-image.client.view.html',
        controller: 'ImagesController',
        controllerAs: 'vm',
        resolve: {
          imageResolve: getImage
        },
        data: {
          pageTitle: 'Image {{ imageResolve.title }}'
        }
      });
  }

  getImage.$inject = ['$stateParams', 'IService'];

  function getImage($stateParams, IService) {
    return IService.get({
      imageId: $stateParams.imageId
    }).$promise;
  }

  function searchImage($stateParams, SearchImagesService) {
    return SearchImagesService.get({
      imageId: $stateParams.imageId
    }).$promise;
  }


}());
*/
