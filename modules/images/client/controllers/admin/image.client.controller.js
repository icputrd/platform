﻿(function () {
  'use strict';

  angular
    .module('images.admin')
    .controller('ImagesAdminController', ImagesAdminController);

  ImagesAdminController.$inject = ['$scope', '$state', '$window', 'imageResolve', 'Authentication', 'Notification'];

  function ImagesAdminController($scope, $state, $window, image, Authentication, Notification) {
    var vm = this;

    vm.image = image;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Image
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.image.$remove(function() {
          $state.go('admin.images.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Image deleted successfully!' });
        });
      }
    }

    // Save Image
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.imageForm');
        return false;
      }

      // Create a new image, or update the current instance
      vm.image.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.images.list'); // should we send the User to the list or the updated Image's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Image saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Image save error!' });
      }
    }
  }
}());
