﻿(function () {
  'use strict';

  angular
    .module('images.admin')
    .controller('ImagesAdminListController', ImagesAdminListController);

  ImagesAdminListController.$inject = ['ImagesService'];

  function ImagesAdminListController(ImagesService) {
    var vm = this;

    vm.images = ImagesService.query();
  }
}());
