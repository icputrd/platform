(function (app) {
  'use strict';

  app.registerModule('images', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('images.admin', ['core.admin']);
  app.registerModule('images.admin.routes', ['core.admin.routes']);
  app.registerModule('images.services');
  app.registerModule('images.routes', ['ui.router', 'core.routes', 'images.services']);
}(ApplicationConfiguration));
