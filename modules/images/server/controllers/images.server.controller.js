'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Image = mongoose.model('Image'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an image
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));
  var image = new Image(req.body);
  image.user = req.user;
  image.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(image);
    }
  });
};

/**
 * Show the current image
 */
exports.read = function (req, res) {
  console.log('read - ' + JSON.stringify(req.body));
  var image = req.image ? req.image.toJSON() : {};
  // Add a custom field to the Image, for determining if the current User is the "owner".
  image.isCurrentUserOwner = !!(req.user && image.user && image.user._id.toString() === req.user._id.toString());
  res.json(image);
};
/**
 * Update an image
 */
exports.update = function (req, res) {
  console.log('update- ' + JSON.stringify(req.body));
  var image = req.image;
  image.image = req.body.image;
  image.image = req.body.image;
  image.location = req.body.location;
  image.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(image);
    }
  });
};

/**
 * Delete an image
 */
exports.delete = function (req, res) {
  var image = req.image;
  image.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(image);
    }
  });
};

/**
 * List of Images
 */
exports.list = function (req, res) {
  console.log('list- ' + req);
  console.log('What is going on here');
  Image.find({ 'days': 30 }).exec(function (err, images) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(images);
    }
  });
};
/**
 * Image middleware
 */
exports.imageByID = function (req, res, next, id) {
  console.log('imageByID- ' + JSON.stringify(req.body));
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Image is invalid'
    });
  }

  Image.findById(id).populate('user', 'displayName').exec(function (err, image) {
    if (err) {
      return next(err);
    } else if (!image) {
      return res.status(404).send({
        message: 'No image with that identifier has been found'
      });
    }
    req.image = image;
    next();
  });
};

// using app.param to deconstruct url to get image for search.
exports.imageID = function (req, res, next, id) {
  console.log('imageByID- ' + JSON.stringify(req.body));
  var plus = 'http://localhost:3000/';
  plus = plus + id;
  Image.find({ 'image': plus }).populate('user', 'displayName').exec(function (err, image) {
    if (err) {
      return next(err);
    } else if (!image) {
      return res.status(404).send({
        message: 'No image with that identifier has been found'
      });
    }
    req.image = image;
    next();
  });
};
// Copy exports.read
exports.readImage = function (req, res) {
  console.log('read - ' + JSON.stringify(req.body));
  var image = req.image;
  res.json(image);
};

