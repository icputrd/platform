'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Image Schema
 */
var ImageSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  utid: {
    type: String,
    default: '',
    trim: true,
    index: true
  },
  url: {
    type: String,
    default: '',
    index: true,
    required: 'URL cannot be blank'
  },
  days: {
    type: Number,
    default: ''
  },
  dates: {
    type: Date,
    default: ''
  },
  pdates: {
    type: Date,
    default: ''
  }
});

mongoose.model('Image', ImageSchema);
