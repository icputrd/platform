'use strict';

/**
 * Module dependencies
 */
var logs = require('../controllers/logs.server.controller');

module.exports = function (app) {
  app.route('/api/logs').post(logs.create);
};
