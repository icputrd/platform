import pymongo
from pymongo import MongoClient
from datetime import datetime
import sys

client = MongoClient("dbNew")
db = client.get_database("mean-dev")

data = open(sys.argv[1]).readlines()
for img in data:


    now = datetime.now()
    
    image = "/"
    path = img.split(":")[0].strip().split("/")[-3:]
    image = "/"+image.join(path)
    if "icon" not in image:
        image = image.replace(".JPG",".icon.JPG")
    label = img.split(":")[1].strip()
    db.stages.insert({
            "image" : image,
            "label" : label,
            "bodypart": label,
            "created" : now,
            "__v" : 0
        })
