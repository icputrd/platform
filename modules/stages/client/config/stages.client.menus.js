(function () {
  'use strict';

  angular
    .module('stages')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Stages',
      state: 'stages',
      type: 'dropdown',
      roles: ['*']
    });

		// Add dropdown list item
    menuService.addSubMenuItem('topbar', 'stages', {
      title: 'Browse stages',
      state: 'stages.labels',
      roles: ['*']
    });
  }
}());
