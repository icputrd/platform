(function () {
  'use strict';

  angular
    .module('stages.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('stages', {
        abstract: true,
        url: '/stages',
        template: '<ui-view/>'
      })
      .state('stages.labels', {
        url: '',
        templateUrl: '/modules/stages/client/views/labels-list.client.view.html',
        controller: 'StageListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Stage Labels'
        }
      })
      .state('stages.images', {
        url: '/:label',
        templateUrl: '/modules/stages/client/views/image-list.client.view.html',
        controller: 'StageImagesListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Stage Images'
        }
      });
  }
}());
