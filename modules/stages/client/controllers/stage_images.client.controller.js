(function () {
  'use strict';

  angular
    .module('stages')
    .controller('StageImagesListController', StageImagesListController);

  StageImagesListController.$inject = ['StageImagesService', '$stateParams', '$scope', '$http'];

  function StageImagesListController(StageImagesService, $stateParams, $scope, $http) {
    var vm = this;
    vm.images = StageImagesService.query({
    	label: $stateParams.label
    });
    vm.selected = [];
    console.log(vm.images);
    vm.labels = ["M-1","M-2","M-3","M-4","G-1","G-2","G-3","G-4","G-5","G-6","DN-1","DN-2","DN-3","DN-4","DN-5"]
    vm.labelsM = ["M-1","M-2","M-3","M-4"]
    vm.labelsG = ["G-1","G-2","G-3","G-4","G-5","G-6"]
    vm.labelsDN = ["DN-1","DN-2","DN-3","DN-4","DN-5"]
    //vm.labels = ["h-1", "h-2", "h-3", "h-4", "h-5", "t-1", "t-2", "t-3", "t-4", "t-5", "l-1", "l-2", "l-3", "l-4", "l-5", "remove", "checked"];
    //vm.labels2 = ["remove", "head", "torso", "back", "backside", "hips", "arm", "hand", "legs", "foot", "knee", "fullbody", "plastic", "shade", "stake", "other"];
    $scope.changeLabel = function(i) {
    	console.log(i);
        const lastSlash = window.location.pathname.lastIndexOf('/');
        const prevStageName = window.location.pathname.substr(lastSlash + 1);
	$http.post('/api/stages/' + i.label, {
		data: {
			label: i.label,
			oldLabel: prevStageName,
			selectedImages: [i.image],
		}
	}).then(function (response) {
		if (response.status === 200) {
			vm.images = StageImagesService.query({
				label: $stateParams.label
			});
		}	
	});
    }

    $scope.changeAllLabels = function() {
        const lastSlash = window.location.pathname.lastIndexOf('/');
        const prevStageName = window.location.pathname.substr(lastSlash + 1);
        $http.post('/api/stages/' + vm.fullStageName, {
          data: {
                label: vm.fullStageName,
                oldLabel: prevStageName,
		selectedImages: vm.selected,
          }
        }).then(function (response){
		console.log(response);
		if (response.status === 200) {
			vm.images = StageImagesService.query({
				label: $stateParams.label
			});
		}
	});
    }
    $scope.currentPage = 0;
    $scope.pageSize = 45;
    $scope.selectAll = function() {
   	vm.images.slice($scope.currentPage*$scope.pageSize,$scope.currentPage*$scope.pageSize+$scope.pageSize).forEach(i => vm.selected.push(i.image)); 
    }

    $scope.deselectAll = function() {
    	vm.selected = []; 
    }

    $scope.selectImage = function(img) {
	const index = vm.selected.indexOf(img);
   	if (index >= 0) {
		vm.selected.splice(index, 1);
	} else {
		vm.selected.push(img);	
	}
    }

    $scope.currentPage = 0;
    $scope.pageSize = 45;
    $scope.numberOfPages= () => {
        return Math.ceil(vm.images.length / $scope.pageSize);
    }
  
  }
}());
