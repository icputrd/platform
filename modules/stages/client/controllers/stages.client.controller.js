(function () {
  'use strict';

  angular
    .module('stages')
    .controller('StageListController', StageListController);

  StageListController.$inject = ['StagesService', '$scope'];

  function StageListController(StagesService, $scope) {
    var vm = this;
    vm.stage = StagesService.query();
    /*
    $scope.filterItems = {
      'numDays': true
    };
    $scope.testFilter = function(browse) {
      console.log(browse._id);
      return $scope.filterItems[browse.numDays];
    };
    */
    console.log(vm.stage);
  }
}());
