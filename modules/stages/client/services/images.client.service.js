(function () {
  'use strict';

  angular
    .module('stages.services')
    .factory('StageImagesService', StageImagesService);

  StageImagesService.$inject = ['$resource', '$log'];

  function StageImagesService($resource, $log) {
    console.log('stage images service');
    var images = $resource('/api/stages/:label', {}, {
      update: {
        method: 'PUT'
      }
    });
    console.log('images');
    return images;
  }
}());
