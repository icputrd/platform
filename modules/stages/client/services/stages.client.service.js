(function () {
  'use strict';

  angular
    .module('stages.services')
    .factory('StagesService', StagesService);

  StagesService.$inject = ['$resource', '$log'];

  function StagesService($resource, $log) {
    console.log('stages service');
    var Stages = $resource('/api/stages', {
      id: '@_id',
      label: 'label'
    }, {
      update: {
        method: 'PUT'
      }
    });
    console.log('Stages');
    return Stages;
  }
}());
