(function (app) {
  'use strict';

  // The core module is required for special route handling;
  // see /core/client/config/core.client.routes
  app.registerModule('stages', ['core']);
  app.registerModule('stages.services');
  app.registerModule('stages.routes', ['ui.router', 'core.routes', 'stages.services']);
}(ApplicationConfiguration));
