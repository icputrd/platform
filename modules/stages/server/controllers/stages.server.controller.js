'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Stage = mongoose.model('Stages'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * List of Stages
 */
exports.list = function (req, res) {
  Stage.distinct('label').sort().exec(function (err, stages) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(stages);
    }
  });
};

/** *********************************************************************************************************/
/**
 * update a stage
 */
exports.update = function (req, res) {
  console.log('update- ' + JSON.stringify(req.body));
  var stage = req.stage;
  stage.stage_name = req.body.stage_name;
  stage.image = req.body.image;
  stage.location = req.body.location;
  stage.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(stage);
    }
  });
};
/**
 * Create a stage
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));

  var data = req.body.data;
  var stage = new Stage(data);

  Stage.find({ 'image': stage.image }).exec(function (err, stages) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      if (stages.length !== 0) {
        stages[0].stage_name = stage.stage_name;
        stages[0].label = stage.label;
        stage = stages[0];
      }
      stage.save(function (err) {
        if (err) {
          return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.json(stage);
        }
      });
    }
  });
};

exports.move = function(req, res) {
	const oldLabel = req.body.data.oldLabel;
	const selectedImages = req.body.data.selectedImages;
	Stage.find({'label': oldLabel}).exec(function (err, stages) {
		if (err) {
		    return res.status(422).send({
			message: errorHandler.getErrorMessage(err)
		    });
		} else {
			for (let i = 0; i < stages.length; i++){
				if (selectedImages.indexOf(stages[i].image) >= 0) {
					stages[i].label = req.body.data.label;
					stages[i].save(function(err) {
						if (err) {
						    return res.status(422).send({
							message: errorHandler.getErrorMessage(err)
						    });
						}
					});
				}
			}
		}
	});
	res.json({'status': 'done'});
};

/**
 * Show the current stage
 */
exports.read = function (req, res) {
  var stage = req.stage ? JSON.stringify(req.body) : {};
  stage = req.stage;
  res.json(stage);
};

/**
 * Stage middleware
 */
exports.stageByID = function (req, res, next, id) {
  console.log('stageByID- ' + JSON.stringify(req.body));
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Stage is invalid'
    });
  }

  Stage.findById(id).exec(function (err, stage) {
    if (err) {
      return next(err);
    } else if (!stage) {
      return res.status(404).send({
        message: 'No stage with that identifier has been found'
      });
    }
    req.stage = stage;
    next();
  });
};

/**
 * Label middleware
 */
exports.label = function (req, res, next, label) {
  console.log('label- ' + JSON.stringify(req.body));
  Stage.find({ label: label }).exec(function (err, stage) {
    if (err) {
      return next(err);
    } else if (!stage) {
      return res.status(404).send({
        message: 'No stage with that label has been found'
      });
    }
    req.stage = stage;
    next();
  });
};

