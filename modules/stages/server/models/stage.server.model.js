'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Stage Schema
 */
var StageSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  label: {
    type: String,
    default: ''
  },
  bodypart: {
    type: String,
    default: ''
  },
  image: {
    type: String,
    default: ''
  }
});


var createCompoundIndex = function(db, callback) {
//  Get the documents collection
  var collection = db.collection('stages');
  // Create the index
  collection.createIndex(
    { '$**': 'text' }, function(err, result) {
      console.log(result);
      callback(result);
    });
};
mongoose.model('Stages', StageSchema);
