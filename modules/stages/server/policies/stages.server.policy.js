'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Stage Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/stages',
      permissions: '*'
    }, {
      resources: '/api/stages/create',
      permissions: '*'
    }, {
      resources: '/api/stages/:stageId',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/stages',
      permissions: ['*']
    }, {
      resources: '/api/stages/image/:photo',
      permissions: '*'
    }, {
      resources: '/api/stages/:stageId',
      permissions: ['*']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/stages',
      permissions: ['*']
    }, {
      resources: '/api/stages/image/:photo',
      permissions: '*'
    }, {
      resources: '/api/stages/:stageId',
      permissions: ['*']
    }]
  }]);
};

/**
 * Check If Cluseter Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];
  console.log(req.user);

  // If an stage is being processed and the current user created it then allow any manipulation
  if (req.stage && req.user) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
