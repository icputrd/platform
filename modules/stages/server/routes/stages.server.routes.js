'use strict';

/**
 * Module dependencies
 */
var stagesPolicy = require('../policies/stages.server.policy'),
  stages = require('../controllers/stages.server.controller');

module.exports = function (app) {
  app.route('/api/stages').all(stagesPolicy.isAllowed).get(stages.list).post(stages.create);
  // Single stage routes
  app.route('/api/stages/:label').all(stagesPolicy.isAllowed).get(stages.read).post(stages.move);
  // Finish by binding the stage middleware
  app.param('stageId', stages.stageByID);
  app.param('label', stages.label);
};
