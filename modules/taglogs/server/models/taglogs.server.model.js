'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * TagLog Schema
 */
var TagLogSchema = new Schema({
  searchstring: {
    type: String,
    default: ''
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  created: {
    type: Date,
    default: Date.now
  }

});

mongoose.model('TagLog', TagLogSchema);
