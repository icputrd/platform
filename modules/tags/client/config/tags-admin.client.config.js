﻿(function () {
  'use strict';

  // Configuring the Tags Admin module
  angular
    .module('tags.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Tags',
      state: 'admin.tags.list'
    });
  }
}());
