﻿(function () {
  'use strict';

  angular
    .module('tags.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.tags', {
        abstract: true,
        url: '/tags',
        template: '<ui-view/>'
      })
      .state('admin.tags.list', {
        url: '',
        templateUrl: '/modules/tags/client/views/admin/list-tags.client.view.html',
        controller: 'TagsAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.tags.create', {
        url: '/create',
        templateUrl: '/modules/tags/client/views/admin/form-tag.client.view.html',
        controller: 'TagsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          tagResolve: newTag
        }
      })
      .state('admin.tags.edit', {
        url: '/:tagId/edit',
        templateUrl: '/modules/tags/client/views/admin/form-tag.client.view.html',
        controller: 'TagsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          tagResolve: getTag
        }
      });
  }

  getTag.$inject = ['$stateParams', 'TagsService'];

  function getTag($stateParams, TagsService) {
    return TagsService.get({
      tagId: $stateParams.tagId
    }).$promise;
  }

  newTag.$inject = ['TagsService'];

  function newTag(TagsService) {
    return new TagsService();
  }
}());
