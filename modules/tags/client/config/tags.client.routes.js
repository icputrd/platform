(function () {
  'use strict';

  angular
    .module('tags.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('tags', {
        abstract: true,
        url: '/tags',
        template: '<ui-view/>'
      })
      .state('tags.list', {
        url: '',
        templateUrl: '/modules/tags/client/views/list-tags.client.view.html',
        controller: 'TagsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Tags List'
        }
      })
      .state('tags.search', {
        url: '/search',
        templateUrl: '/modules/tags/client/views/search-tags.client.view.html',
        controller: 'SearchTagsController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Tags Search'
        }
      })
      .state('tags.view', {
        url: '/:tagId',
        templateUrl: '/modules/tags/client/views/view-tag.client.view.html',
        controller: 'TagsController',
        controllerAs: 'vm',
        resolve: {
          tagResolve: getTag
        },
        data: {
          pageTitle: 'Tag {{ tagResolve.title }}'
        }
      });
  }

  getTag.$inject = ['$stateParams', 'TagsService'];

  function getTag($stateParams, TagsService) {
    return TagsService.get({
      tagId: $stateParams.tagId
    }).$promise;
  }

  function searchTag($stateParams, SearchTagsService) {
    return SearchTagsService.get({
      tagId: $stateParams.tagId
    }).$promise;
  }


}());
