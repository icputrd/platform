/* eslint-disable indent, no-trailing-spaces */

(function () {
  'use strict';

  angular
    .module('tags')
    .controller('SearchTagsController', SearchTagsController);

  SearchTagsController.$inject = ['$scope', 'Authentication', 'TagsService', 'tagFactory', '$http'];

  function SearchTagsController($scope, Authentication, TagsService, tagFactory, $http) {
    var vm = this;

    $scope.message = '';
    $scope.message2 = '';
    vm.authentication = Authentication;
    vm.count = 0;
    $scope.searchString = '';
//    $scope.items = [1, 2, 3, 4, 5];
    $scope.AndOr = ['and', 'or'];
    $http.get('/checkbox_tags_list.json').then(function(response) {
      $scope.items = response.data;
    });
    $scope.items = ['fluid', 'purge', 'mummification', 'adult fly', 'eggs', 'larvae', 'discoloration', 'adipocere', 'scavenging', 'skin slippage', 'lividity', 'bloat', 'marbling', 'tattoo', 'mold', 'insect'];
    $scope.anatomic = ['leg', 'arm', 'bone', 'head', 'skin', 'foot', 'eye', 'nose', 'hand', 'mouth', 'genitals', 'hair', 'ear'];
    vm.stags = TagsService.query();
// Used to determine if toggle is "and" or reset.
    var toggleKeeper = 1;
    var toggleArray = [];
    var result = [];
    var resultForSubsearch = [];
    var resultForSs = [];
    var type_of_search = '1';
    var stuffThisARay = [];
    var toggleFirst = 0;
// //////////////////////////////////////////////////
// //////////////////SEARCH//////////////////////////
// //////////////////////////////////////////////////
    $scope.toggle = function(item, selected) {
      console.log('Search item Toggle:', item + ' ' + selected);
      if (toggleFirst === 0) {
        console.log('Tag Search:', 'ToggleFirstZero-True');
        result = tagFactory.find(vm.stags, item);
        stuffThisARay.push(item.toLowerCase());
        toggleFirst = 100;
      } else {
        console.log('Tag Search:', 'ToggleFirstZero- False');
        console.log('stuffThisArraySize:', stuffThisARay.length);
        angular.forEach(stuffThisARay, function(item1) {
          if (item1.toLowerCase().indexOf(item) === -1) {
            stuffThisARay.push(item.toLowerCase());
          } else {
            stuffThisARay.splice(stuffThisARay.indexOf(item), 1);
          }
        });
        console.log('stuffThisARaySize:', stuffThisARay.length);
        if (!stuffThisARay) {
          $scope.clear();
          // toggleFirst = 0; // added by Rayhan for debug
        } else {
          if (type_of_search === '1') result = $scope.betterSearchAND(stuffThisARay);
          else if (type_of_search === '0') result = $scope.betterSearchOR(stuffThisARay);
        }
      }
      vm.tags = result;
      vm.count = vm.tags.length;
      resultForSs = resultForSubsearch = result;
      $scope.searchString = '';
    };
    $scope.selected_items = [];
    $scope.checkBoxToggle = function(item) {
      var idx = $scope.selected_items.indexOf(item);
      if (idx > -1) {
        $scope.selected_items.splice(idx, 1);
      } else {
        $scope.selected_items.push(item);
      }
      console.log('Selected Items:', $scope.selected_items);
    };
    $scope.check_search = function() {
      if (type_of_search === '1') result = $scope.betterSearchAND($scope.selected_items);
      else if (type_of_search === '0') result = $scope.betterSearchOR($scope.selected_items);
      vm.tags = null;
      vm.count = 0;
      vm.tags = result;
      vm.count = vm.tags.length;
    };
 $scope.andOr = function(item) {
      type_of_search = item;
      console.log(item);
    };
    $scope.search = function() {
      console.log(type_of_search);
      var searchString = $scope.searchString;
      

      if (!$scope.searchString) {
        console.log('What doesn\'t go here');
        return '';
      }
//      console.log($scope.searchString);
      // var searchArray = deconstructString(searchString);
      // searchArray = ['eggs', 'adult fly'];
      var searchArray = searchString.split(',');
      var index;
      for (index = 0; index < searchArray.length; index++) {
        searchArray[index] = searchArray[index].trim();
      }

      if (type_of_search === '1') result = $scope.betterSearchAND(searchArray);
      else if (type_of_search === '0') result = $scope.betterSearchOR(searchArray);

//      result = tagFactory.find(vm.stags, $scope.searchString);
      vm.tags = null;
      vm.count = 0;
      vm.tags = result;
      vm.count = vm.tags.length;
      console.log(vm.count);
      resultForSs = resultForSubsearch = result;
      $scope.searchString = '';
    };
// refineSearch //////////////////////////////////////////////
    $scope.refineSearch = function() {
      var searchString = $scope.searchString;
      var tagCount = 0;
      $scope.searchString = $scope.searchString.toLowerCase();
      deconstructString(searchString);
      var small = $scope.searchString.toLowerCase();
      var result2 = [];
      var rFS = resultForSs; // Array
      vm.tags = null;
      $scope.clear();
      angular.forEach(rFS, function(item) {
        if (item.tag.toLowerCase().indexOf(small) !== -1) {
          result2.push(item);
          tagCount++;
        }
      });
      vm.tags = result2;
      resultForSs = result2;
      vm.count = tagCount;
      return 'img';
    };
// clear /////////////////////////////////////////////////////
    $scope.clear = function() {
      $scope.searchString = '';
      resultForSubsearch = null;
      result = null;
      vm.count = null;
      vm.tags = null;
      toggleFirst = 0;
    };

    $scope.betterSearchOR = function(aRay) {
      var total = [];
      console.log(aRay);
      for (var i = 0; i < aRay.length; i++) {
        console.log(i);
        var result = tagFactory.find(vm.stags, aRay[i]);
        console.log(result);
        for (var j = 0; j < result.length; j++) {
          console.log(result[j]);
          total.push(result[j]);
        }
      }
      console.log('leaving searchOr', total);
      var unique_list = get_unique_list(total);
      // return total;
      return unique_list;
    };
$scope.betterSearchAND = function(aRay) {
      var total = [];
      var total2 = [];
      console.log('betterSearchAnd-aRay', aRay);
      for (var i = 0; i < aRay.length; i++) {
        console.log(aRay[i]);
        total = [];
        var result = tagFactory.find(vm.stags, aRay[i]);
        if (i === 0) {
          total = total2 = result;
          // console.log(result);
        } else {
          for (var j = 0; j < result.length; j++) {
            for (var k = 0; k < total2.length; k++) {
              if (total2[k]._id.indexOf(result[j]._id) !== -1) {
                console.log(result[j]);
                // console.log(total2[k]);
                total.push(result[j]);
              }
            }
          }
          total2 = total;
        }
      }
      console.log('leaving searchAnd', total);
      var unique_list = get_unique_list(total);
      // return total;
      return unique_list;
    };
  }
  function get_unique_list(total) {
    var unique_list = [];
    for (var t = 0; t < total.length; t++) {
      var in_list = false;
      for (var u = 0; u < unique_list.length; u++) {
        if (unique_list[u].url === total[t].url) {
          console.log('Repeated:', total[t].url);
          in_list = true;
          break;
        }
      }
      if (!in_list) {
        unique_list.push(total[t]);
      }
    }
    return unique_list;
  }
  function deconstructString(sS) {
    var firstSplit = sS.split(' ');
    return firstSplit;
  }
}());

