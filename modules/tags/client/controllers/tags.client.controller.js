(function () {
  'use strict';

  angular
    .module('tags')
    .controller('TagsController', TagsController);

  TagsController.$inject = ['$scope', 'tagResolve', 'Authentication'];

  function TagsController($scope, tag, Authentication) {
    var vm = this;

    vm.tag = tag;
    vm.authentication = Authentication;

  }
}());
