(function () {
  'use strict';

  angular
    .module('tags.services')
    .factory('tagFactory', tagFactory);

  tagFactory.$inject = ['$resource', '$log', '$http'];

  function tagFactory($resource, $log, $http) {
    var url = 'api/tags';
    var pop = 'http://localhost:3000/api/tags';
    function list() {
      return $http.get(pop)
        .then(function(response) {
          return response.data;
        });
    }

    function find(tagArray, sString) {
      var image = [];
      var tagCount = 0;

      console.log('tagArray', tagArray);

      tagCount = 0;
      var serverInfo = [];
      angular.forEach(tagArray, function(item) {
        if (item.tag.toLowerCase().indexOf(sString) !== -1) {
          tagCount++;

          item.icon = '/' + item.image.replace(/JPG/, 'icon.JPG');
          item.url = '/' + item.image.replace(/JPG/, 'html');

          // item.image = item.image.replace(/http:\/\/localhost:3000\//, '');
          // item.image = item.image.replace(/.*\//, '');
          image.push(item);
        }
      });
      return image;
    }
    function add() {
      return $http.get(url);
    }
    return {
      add: add,
      find: find,
      list: list
    };
  }
}());

