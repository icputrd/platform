(function () {
  'use strict';

  angular
    .module('tags.services')
    .factory('TagsService', TagsService);

  TagsService.$inject = ['$resource', '$log'];

  function TagsService($resource, $log) {
    console.log('inside service');
    var Tag = $resource('/api/tags/:tagId', {
      tagId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    return Tag;
  }

}());
