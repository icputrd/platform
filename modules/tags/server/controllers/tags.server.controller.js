'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Tag = mongoose.model('Tag'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * List of Tags
 */
exports.list = function (req, res) {
  console.log('list- ' + req);
  Tag.find({}, { 'tag': 1, 'image': 1 }).exec(function (err, tags) {
    if (err) {
      console.log(err);
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(tags);
    }
  });
};

/** *********************************************************************************************************/
/**
 * Create an tag
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));
  var tag = new Tag(req.body);
  tag.user = req.user;
  tag.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(tag);
    }
  });
};

/**
 * Show the current tag
 */
exports.read = function (req, res) {
  console.log('read - the book the teacher STATES!');
  var tag = req.tag ? JSON.stringify(req.body) : {};
  console.log('TAG     ', req.tag);
  // Add a custom field to the Tag, for determining if the current User is the "owner".
//  tag.isCurrentUserOwner = !!(req.user && tag.user && tag.user._id.toString() === req.user._id.toString());
  tag = req.tag;
  res.json(tag);
};
/**
 * Update an tag
 */
exports.update = function (req, res) {
  console.log('update- ' + JSON.stringify(req.body));
  var tag = req.tag;
  tag.tag = req.body.tag;
  tag.image = req.body.image;
  tag.location = req.body.location;
  tag.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(tag);
    }
  });
};

/**
 * Delete an tag
 */
exports.delete = function (req, res) {
  var tag = req.tag;
  tag.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(tag);
    }
  });
};

/**
 * Tag middleware
 */
exports.tagByID = function (req, res, next, id) {
  console.log('tagByID- ' + JSON.stringify(req.body));
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Tag is invalid'
    });
  }

  Tag.findById(id).populate('user', 'displayName').exec(function (err, tag) {
    if (err) {
      return next(err);
    } else if (!tag) {
      return res.status(404).send({
        message: 'No tag with that identifier has been found'
      });
    }
    req.tag = tag;
    next();
  });
};

exports.imageID = function (req, res, next, id) {
  console.log('imageID- ' + JSON.stringify(req.body));
  var plus = 'http://localhost:3000/';
  var filename = id.replace('image/', '');
  plus = plus + filename;
  console.log('imageID-   ' + plus, ':  ', id);
  console.log('user', req.user);
  // var saraID = 'ObjectId(\"5b0471ddf1e6500030825d24\")';
  var saraID = '5b0471ddf1e6500030825d24';
  var kelleyID = '5ee120f70af99a0034138d25';
  /* Tag.find({ 'image': plus }).exec(function (err, tag) {
    if (err) {
      return next(err);
    } else if (!tag) {
      return res.status(404).send({
        message: 'No tag with that identifier has been found'
      });
    }
    req.tag = tag;
    next();
  }); */
  if (String(req.user._id) !== String(saraID) && String(req.user._id) !== String(kelleyID)) {
    Tag.find({ 'image': id }).find({ 'user': req.user }).exec(function (err, tag) {
      if (err) {
        return next(err);
      } else if (!tag) {
        return res.status(404).send({
          message: 'No tag with that identifier has been found'
        });
      }
      req.tag = tag;
      next();
    });
  } else {
    Tag.find({ 'image': id }).exec(function (err, tag) {
      if (err) {
        return next(err);
      } else if (!tag) {
        return res.status(404).send({ message: 'No tag with that identifier has been found' });
      }

      req.tag = tag;
      next();
    });
  }
};
exports.readImage = function (req, res) {
  console.log('readImage - {}');
  var tag = req.tag ? JSON.stringify(req.body) : {};
  console.log('TAG     ', req.tag);
  // Add a custom field to the Tag, for determining if the current User is the "owner".
//  tag.isCurrentUserOwner = !!(req.user && tag.user && tag.user._id.toString() === req.user._id.toString());
  tag = req.tag;
  res.json(tag);
};
