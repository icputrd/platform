﻿(function () {
  'use strict';

  // Configuring the Utags Admin module
  angular
    .module('utags.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Utags',
      state: 'admin.utags.list'
    });
  }
}());
