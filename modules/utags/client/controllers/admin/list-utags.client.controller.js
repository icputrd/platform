﻿(function () {
  'use strict';

  angular
    .module('utags.admin')
    .controller('UtagsAdminListController', UtagsAdminListController);

  UtagsAdminListController.$inject = ['UtagsService'];

  function UtagsAdminListController(UtagsService) {
    var vm = this;

    vm.utags = UtagsService.query();
  }
}());
