﻿(function () {
  'use strict';

  angular
    .module('utags.admin')
    .controller('UtagsAdminController', UtagsAdminController);

  UtagsAdminController.$inject = ['$scope', '$state', '$window', 'utagResolve', 'Authentication', 'Notification'];

  function UtagsAdminController($scope, $state, $window, utag, Authentication, Notification) {
    var vm = this;

    vm.utag = utag;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Utag
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.utag.$remove(function() {
          $state.go('admin.utags.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Utag deleted successfully!' });
        });
      }
    }

    // Save Utag
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.utagForm');
        return false;
      }

      // Create a new utag, or update the current instance
      vm.utag.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.utags.list'); // should we send the User to the list or the updated Utag's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Utag saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Utag save error!' });
      }
    }
  }
}());
