(function () {
  'use strict';

  angular
    .module('utags')
    .controller('UtagsController', UtagsController);

  UtagsController.$inject = ['$scope', 'utagResolve', 'Authentication'];

  function UtagsController($scope, utag, Authentication) {
    var vm = this;

    vm.utag = utag;
    vm.authentication = Authentication;

  }
}());
