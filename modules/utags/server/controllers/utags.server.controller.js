'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Utag = mongoose.model('Utag'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an utag
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));
  var utag = new Utag(req.body);
  utag.user = req.user;
  utag.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(utag);
    }
  });
};

/**
 * Show the current utag
 */
exports.read = function (req, res) {
  console.log('read - ' + JSON.stringify(req.body));
  var utag = req.utag ? req.utag.toJSON() : {};
  // Add a custom field to the Utag, for determining if the current User is the "owner".
  utag.isCurrentUserOwner = !!(req.user && utag.user && utag.user._id.toString() === req.user._id.toString());
  res.json(utag);
};
/**
 * Update an utag
 */
exports.update = function (req, res) {
  console.log('update- ' + JSON.stringify(req.body));
  var utag = req.utag;
  utag.utag = req.body.utag;
  utag.image = req.body.image;
  utag.location = req.body.location;
  utag.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(utag);
    }
  });
};

/**
 * Delete an utag
 */
exports.delete = function (req, res) {
  var utag = req.utag;
  utag.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(utag);
    }
  });
};

/**
 * List of Utags
 */
exports.list = function (req, res) {
  console.log('list- ' + req);
  Utag.find().sort('-created').populate('user', 'displayName').exec(function (err, utags) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(utags);
    }
  });
};
/**
 * Utag middleware
 */
exports.utagByID = function (req, res, next, id) {
  console.log('utagByID- ' + JSON.stringify(req.body));
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Utag is invalid'
    });
  }

  Utag.findById(id).populate('user', 'displayName').exec(function (err, utag) {
    if (err) {
      return next(err);
    } else if (!utag) {
      return res.status(404).send({
        message: 'No utag with that identifier has been found'
      });
    }
    req.utag = utag;
    next();
  });
};

// using app.param to deconstruct url to get image for search.
exports.imageID = function (req, res, next, id) {
  console.log('utagByID- ' + JSON.stringify(req.body));
  var plus = 'http://localhost:3000/';
  plus = plus + id;
  Utag.find({ 'image': plus }).populate('user', 'displayName').exec(function (err, utag) {
    if (err) {
      return next(err);
    } else if (!utag) {
      return res.status(404).send({
        message: 'No utag with that identifier has been found'
      });
    }
    req.utag = utag;
    next();
  });
};
// Copy exports.read
exports.readImage = function (req, res) {
  console.log('read - ' + JSON.stringify(req.body));
  var utag = req.utag;
  res.json(utag);
};

