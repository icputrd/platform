'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Utag Schema
 */
var UtagSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  utag: {
    type: String,
    default: '',
    trim: true,
    required: 'Utag cannot be blank'
  },
  image: {
    type: String,
    default: ''
  },
  utid: {
    type: String,
    default: ''
  },
  location: {
    type: String,
    default: ''
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Utag', UtagSchema);
