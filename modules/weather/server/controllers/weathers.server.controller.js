'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Weather = mongoose.model('Weather'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an weather
 */
exports.create = function (req, res) {
  console.log('create - ' + JSON.stringify(req.body));
  var weather = new Weather(req.body);
  weather.user = req.user;
  weather.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(weather);
    }
  });
};

/**
 * Show the current weather
 */
exports.read = function (req, res) {
  console.log('read - the book the teacher STATES!');
  var weather = req.weather ? JSON.stringify(req.body) : {};
  console.log('TAG     ', req.weather);
  // Add a custom field to the Weather, for determining if the current User is the "owner".
//  weather.isCurrentUserOwner = !!(req.user && weather.user && weather.user._id.toString() === req.user._id.toString());
  weather = req.weather;
  res.json(weather);
};
/**
 * Update an weather
 */
exports.update = function (req, res) {
  console.log('update- ' + JSON.stringify(req.body));
  var weather = req.weather;
  weather.weather = req.body.weather;
  weather.image = req.body.image;
  weather.location = req.body.location;
  weather.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(weather);
    }
  });
};

/**
 * Delete an weather
 */
exports.delete = function (req, res) {
  var weather = req.weather;
  weather.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(weather);
    }
  });
};

/**
 * List of Weathers
 */
exports.list = function (req, res) {
  console.log('list- ' + req);
  Weather.find().sort('-created').populate('user', 'displayName').exec(function (err, weathers) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(weathers);
    }
  });
};
/**
 * Weather middleware
 */
exports.weatherByID = function (req, res, next, id) {
  console.log('weatherByID- ' + JSON.stringify(req.body));
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Weather is invalid'
    });
  }

  Weather.findById(id).populate('user', 'displayName').exec(function (err, weather) {
    if (err) {
      return next(err);
    } else if (!weather) {
      return res.status(404).send({
        message: 'No weather with that identifier has been found'
      });
    }
    req.weather = weather;
    next();
  });
};


exports.imageID = function (req, res, next, id) {
  console.log('imageID- ' + JSON.stringify(req.body));
  var plus = 'http://localhost:3000/';
  var filename = id.replace('image/', '');
  plus = plus + filename;
  console.log('imageID-   ' + plus, ':  ', id);
  var tsaulID = 'ObjectId(\"5880ecda81d60f0c9bde0676\")';
  Weather.find({ 'image': plus }).find({ 'user': req.user }).exec(function (err, weather) {
//  Weather.find({ 'image': plus }).exec(function (err, weather) {
    if (err) {
      return next(err);
    } else if (!weather) {
      return res.status(404).send({
        message: 'No weather with that identifier has been found'
      });
    }
    req.weather = weather;
    next();
  });
};
exports.readImage = function (req, res) {
  console.log('readImage - {}');
  var weather = req.weather ? JSON.stringify(req.body) : {};
  console.log('TAG     ', req.weather);
  // Add a custom field to the Weather, for determining if the current User is the "owner".
//  weather.isCurrentUserOwner = !!(req.user && weather.user && weather.user._id.toString() === req.user._id.toString());
  weather = req.weather;
  res.json(weather);
};
