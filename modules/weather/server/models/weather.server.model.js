'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Weather Schema
 */
var WeatherSchema = new Schema({
  dt: {
    type: String,
    default: ''
  },
  dt_iso: {
    type: Date,
    default: ''
  },
  city_id: {
    type: Number,
    default: ''
  },
  city_name: {
    type: String,
    default: ''
  },
  lat: {
    type: Number,
    default: ''
  },
  lon: {
    type: Number,
    default: ''
  },
  temp: {
    type: Number,
    default: ''
  },
  temp_min: {
    type: Number,
    default: ''
  },
  temp_max: {
    type: Number,
    default: ''
  },
  pressure: {
    type: Number,
    default: ''
  },
  sea_level: {
    type: Number,
    default: ''
  },
  gmd_level: {
    type: Number,
    default: ''
  },
  humidity: {
    type: Number,
    default: ''
  },
  wind_speed: {
    type: Number,
    default: ''
  },
  wind_deg: {
    type: Number,
    default: ''
  },
  rain_1h: {
    type: Number,
    default: ''
  },
  rain_3h: {
    type: Number,
    default: ''
  },
  rain_24h: {
    type: Number,
    default: ''
  },
  rain_today: {
    type: Number,
    default: ''
  },
  snow_1h: {
    type: Number,
    default: ''
  },
  snow_3h: {
    type: Number,
    default: ''
  },
  snow_24h: {
    type: Number,
    default: ''
  },
  snow_today: {
    type: Number,
    default: ''
  },
  clouds_all: {
    type: Number,
    default: ''
  },
  weather_id: {
    type: Number,
    default: ''
  },
  weather_main: {
    type: String,
    default: ''
  },
  weather_description: {
    type: String,
    default: ''
  },
  weather_icon: {
    type: String,
    default: ''
  }
});


mongoose.model('Weather', WeatherSchema);
