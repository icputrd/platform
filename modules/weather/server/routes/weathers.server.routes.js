'use strict';

/**
 * Module dependencies
 */
var weathersPolicy = require('../policies/weathers.server.policy'),
  weathers = require('../controllers/weathers.server.controller');

module.exports = function (app) {
  app.route('/api/weather/image/:photo').all(weathersPolicy.isAllowed).get(weathers.readImage);
  app.param('photo', weathers.imageID);
  // Weathers collection routes
  app.route('/api/weather').all(weathersPolicy.isAllowed).get(weathers.list).post(weathers.create);
  // Single weather routes
  app.route('/api/weather/:weatherId').all(weathersPolicy.isAllowed).get(weathers.read).put(weathers.update).delete(weathers.delete);
  // Finish by binding the weather middleware
  app.param('weatherId', weathers.weatherByID);
  // New route for image only weathers
};
