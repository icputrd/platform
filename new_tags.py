import pymongo
import os
import sys
import glob
import re

myclient = pymongo.MongoClient("dbNew")
mydb = myclient["mean-dev"]

oldTags = mydb["tags"]
newTags = mydb["new_tags"]

oldTagsList = []
oldTagsList = oldTags.find({"image": {"$exists": True}})
print(oldTagsList.count())
f = open("mappings.txt", "r")
data = f.readlines()

mappings = {}

for line in data:
  line = line[:-1]
  oldid = line.split(" ")[0]
  newid = line[-3:]
  mappings.update({oldid: newid})
  print(oldid)
  print(newid)  


for tag in oldTagsList:
  #print(tag)
  if("Daily" in tag['image']):
    oldPath = tag['image']
    oldPath = oldPath.replace("//", "/", 100)
    newPath = oldPath[20:]
    print(newPath)

    year = newPath.split("/")[1]
    year = year[-2:]
    utid = newPath.split("/")[2]
    if not utid:
        utid = newPath.split("/")[3]
    print("UTID %s" % utid)
    newid = mappings[utid]
    print("New id %s" % newid)
    img = newPath.split("/")[4]
    img = img[:-4]
    print("Img %s" % img)

    if re.match( r'.*_.*_.*_.*', img ) is not None:
      month = img.split("_")[1]
      print("Month %s" % month)
      day = img.split("_")[2]
      print("Day %s" % day)
      
      y = img.split("_")[3]
      y = y.split(" ")[0]
      if("(" in y):
        y = y.split("(")[0]
        print(y)
      
      if(len(y) == 4):
        y = y[-2:]
      elif(len(y) != 2):
        #print("here")
        y = y[:-1]
        y = y[-2:]
      
      yint = int(y) - int(year)
      
      num = img.split("(")[1]
      if(")" in num):
        num = num[:-1]
      #print("Num %s\n" % num)
    path = "img/" + newid + "/" + newid + str(yint) + month + day + "." + num + ".JPG"
    print(path)
    tag["image"] = path
    newTags.insert_one(tag)
    #print(tag)
