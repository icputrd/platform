# ICPUTRD System Installation

The full documentation is [here](https://bitbucket.org/icputrd/platform/src/master/public/ICPUTRDDocumentation.pdf)

A brief version is below 

##  How to Set Up a new Installation of ICPUTRD:
ICPUTRD is developed as a web application based on the MEAN stack framework, and it comes as a docker image which is available on DockerHub. One should go through the following steps to set up a new installation of the ICPUTRD system and start using.

Install Docker:
In the very beginning, the user requires to set up a docker environment on their machine. A detailed guideline to set up docker on Ubuntu machine can be found in the following link.
https://docs.docker.com/v17.09/engine/installation/linux/docker-ce/ubuntu/ 

Docker Pull:
When the environment is ready, we need to pull the ICPUTRD and mongo docker images onto a server where it's going to be run or set up. The following command should be used to pull the docker images.
-	docker pull mongo
-	docker pull icputrd/platform

To list all the docker images run the following command, and search for the two images we just pulled from docker hub.
-	docker images -a

Placing the sample images:
For simplicity and testing purposes, let's create a folder /data/img, and put all the sample images into this folder. Note that, this folder should contain only image files (*.jpg). 

Run Docker container:
Once the system is ready, the next step is to run the Docker container. The icputrd/platform requires the mongo container running before it. If we run the following command, It will run a container named db using the mongo docker image.
- 	docker run --name db -d mongo

Later, we need to run our icputrd/platform container using the following command. Here, rayhan is the username of our local machine, and icputrd is the name for your container which you may change.
docker run -d -v /data/img:/opt/mean.js/public/img -v /home/rayhan:/home/rayhan -p 3000:3000 --link db:db_1 --name icputrd icputrd/platform /bin/startDef.sh rayhan

To go inside into the docker container, open another terminal and run the following command.
- 	docker exec -it icputrd bash
In this new terminal, run the following two commands.
-	export DB_1_PORT_27017_TCP_ADDR=db
-	npm start

Populate the index files:
After going inside the /opt/mean.js/public run the populatePagesSample.sh file. To run the shell script type the following command, and it will create the necessary icon and HTML files.
-	./populatePagesSample.sh


Finally, we can use the ICPUTRD system by browsing the following URL. Users need to create an account first and login to tag the images.

http://localhost:3000/:
