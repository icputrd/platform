import pymongo
import os
import sys
import glob

myclient = pymongo.MongoClient("db")
mydb = myclient["mean-dev"]

mycol = mydb["donors"]

donorList = []

for donor in glob.glob("img" + "/*"):
	dayList = []
	if(os.path.isdir(donor)):
		_id = donor[4:] 	
		#print("\nID: %s" % _id)
		
		for img in sorted(glob.glob(donor + "/*.JPG")):
			date = img[-12:-7]
			#print("Date: %s" % date)
			if(not "icon" in img):
				#print("Img: %s" % img)
				num = img[17:19]
				#print("Number: %s" % num)
				icon = img[:-4] + ".icon.JPG"
				#print("Icon: %s" % icon)
				result = next((item for item in dayList if item["date"] == date), None)
				if(result != None):
					result["images"].append( { "num" : num, "path" : img, "icon": icon } )
				else:
					dayList.append( { "date": date, "icon": icon, "images": [ { "num" : num, "path" : img, "icon": icon } ] } )
					
		d = { "_id": _id, "icon": icon, "days": dayList }
		mycol.insert_one(d)

'''
for x in donorList:
	print("\nID: %s , " % x["_id"], end = '')
	print("Icon: %s , " % x["icon"])
	print("Days:")
	for i in x["days"]:
		print("	Date: %s , " % i["date"], end = '')
		print("Icon: %s " % i["icon"])
		print("		Images:")
		for j in i["images"]:
			print("		Num: %s , " % j["num"], end = '')
			print("Path: %s , " % j["path"], end = '')
			print("Icon: %s " % j["icon"])
'''
