/* File: generate_ids.cpp
	 Author: Dylan Lee
	 Description:
	   Generates a 3 digit hex string for NUM_IDS number of ids.
		 Shuffles the ids to ensure randomness and outputs new hex ids to file "newIds.txt".
		 Used to generate new ids that old UT ids will map to.
*/
#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <random>

using namespace std;

const int NUM_IDS = 4096;

int main(int argc, char** argv) {
	int i;
	ofstream output;
	char idcstr[4];
	string idStr;
	vector<string> newIds;

	output.open("newIds.txt");

	auto rng = default_random_engine {};

	for(i = 0; i < NUM_IDS; i++) {
		sprintf(idcstr, "%x", i);
		idStr = idcstr;
		if(i < 16) idStr = "00" + idStr;
		else if(i < 256) idStr = "0" + idStr;
		newIds.push_back(idStr);
	}
	
	shuffle(begin(newIds), end(newIds), rng);

	for(i = 0; i < NUM_IDS; i++) output << newIds[i] << endl;

	return 0;
}

