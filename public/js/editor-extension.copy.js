/**
 * Extends the Annotorious editor with the Semantic Tagging field.
 * @param {Object} annotator the annotator (provided by the Annotorious framework)
 */
annotorious.plugin.SemanticTagging.prototype._extendEditor = function(annotator) {
  var self = this,
      container = document.createElement('div'),
      idle_timeout,
      MIN_TEXT_LENGTH = 2,   // minimum length annotation must have before being allowed to the NER server
      TRIGGER_CHARS = ". ,", // characters that force an NER lookup
      IDLE_THRESHOLD = 500;  // NER is also done after IDLE_THRESHOLD milliseconds of key idleness

  container.className = 'semtagging-editor-container';

  // Adds a tag
  var addTag = function(annotation, topic, opt_css_class) {
    self._tags[topic.id] = topic;

    var link = document.createElement('a');
    link.style.cursor = 'pointer';
    link.className = 'semtagging-tag semtagging-editor-tag';
    link.innerHTML = topic.title;
    container.appendChild(link);

    var jqLink = jQuery(link);
    if (opt_css_class)
      jqLink.addClass(opt_css_class);

    jqLink.click(function() {
      if (!annotation.tags)
        annotation.tags = [];

      if (jqLink.hasClass('accepted')) {
        // Toggle to 'rejected'
        jqLink.toggleClass('accepted rejected');
        topic.status = 'rejected';
      } else if (jqLink.hasClass('rejected')) {
        // Toggle to 'don't care'
        jqLink.removeClass('rejected');
        delete topic.status;
        var idx = annotation.tags.indexOf(topic);
        if (idx > -1)
          annotation.tags.splice(idx, 1);
      } else {
        // Toggle to 'accepted'
        jqLink.addClass('accepted');
        delete topic.status;
        annotation.tags.push(topic);
      }
    });
  };

  // Does the NER lookup
  var doNER = function(annotation, text) {
    var list = [
      {id: 0, title: 'fluid'},
      {id: 1, title: 'purge'},
      {id: 2, title: 'mummification'},
      {id: 3, title: 'adult fly'},
      {id: 4, title: 'eggs'},
      {id: 5, title: 'larvae'},
      {id: 6, title: 'discoloration'},
      {id: 7, title: 'adipocere'},
      {id: 8, title: 'scavenging'},
      {id: 9, title: 'skin slippage'},
      {id: 10, title: 'lividity'},
      {id: 11, title: 'bloat'},
      {id: 12, title: 'body'},
      {id: 13, title: 'leg'},
      {id: 14, title: 'hand'},
      {id: 15, title: 'scale'},
      {id: 16, title: 'marbling'},
      {id: 17, title: 'whole body'},
      {id: 18, title: 'post-bloat'},
      {id: 19, title: 'maggots'},
      {id: 20, title: 'skeletonization'},
      {id: 21, title: 'mummified'},
      {id: 22, title: 'flies'},
	  {id: 23, title: 'mold'},
	  {id: 24, title: 'insect eggs'},
	  {id: 25, title: 'tattoo'},
	  {id: 26, title: 'head'}

    ];
    if (list.length > 0) {
        jQuery.each(list, function(idx, topic) {
          // Add to cached tag list and UI, if it is not already there
          if (topic.title.startsWith(text) && !self._tags[topic.id])
            addTag(annotation, topic);
        });
     }


    /*jQuery.getJSON(self._ENDPOINT_URI + text, function(data) {
      if (data.detectedTopics.length > 0) {
        jQuery.each(data.detectedTopics, function(idx, topic) {
          // Add to cached tag list and UI, if it is not already there
          if (!self._tags[topic.id])
            addTag(annotation, topic);
        });
      }
    });*/
  };

  // Restarts the keyboard-idleness timeout
  var restartIdleTimeout = function(annotation, text) {
    if (idle_timeout)
      window.clearTimeout(idle_timeout);
    
    idle_timeout = window.setTimeout(function() { doNER(annotation, text); }, IDLE_THRESHOLD);
  };

  // Add a key listener to Annotorious editor (and binds stuff to it)
  annotator.editor.element.addEventListener('keyup', function(event) {
    var annotation = annotator.editor.getAnnotation(),
        text = annotation.text.toLowerCase();

    if (text.length > MIN_TEXT_LENGTH) {
      restartIdleTimeout(annotation, text);

      if (TRIGGER_CHARS.indexOf(text[text.length - 1]) > -1)
        doNER(annotation, text);
    }
  });

  // Final step: adds the field to the editor
  annotator.editor.addField(function(annotation) {
    self._tags = [];
    container.innerHTML = '';
    if (annotation && annotation.tags) { 
      jQuery.each(annotation.tags, function(idx, topic) {
        var css_class = (topic.status == 'rejected') ? 'rejected' : 'accepted';
        addTag(annotation, topic, css_class);
      });
    }
    return container;
  });
}
