// db params
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/icputrd_test';

MongoClient.connect(url, function (err, db) {
  if (err) {
    console.log('Unable to connect to the mongoDB server. Error:', err);
  } else {
    // interact with db
    var collection = db.collection('icputrd_test');
    collection.insert({
      "name":"Person McPerson",
      "position":"Worker"
    });
    // Close connection
    db.close();
  }
});
