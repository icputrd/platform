#!/bin/bash

#create index files for images and html files for curation
ls -d img/* | grep -v "html" | while read donor
do 
	cd $donor
	rm *.html
	ls | grep -v "html" | while read d
	do
		cd $d
		rm index.html
		while read f
		do 
			fname=$(echo "$f" | sed 's/\.JPG//')
			[[ -f "$fname".icon.JPG ]] || convert "$fname".JPG -resize 100x100 "$fname".icon.JPG
			[[ -f "$fname.html" ]] || cat ../../../template.html | sed "s/IMAGE/$fname/" > "$fname.html"
			date=$(echo "$fname" | sed 's/\..*//' | grep -o '.....$')
			number=$(echo "$fname" | grep -o '..$')
			echo '<a href="'"$fname"'.html"><img src="'"$fname"'.icon.JPG" width=100 height=100></a>' >> index.html
		done <<< $( ls *.JPG | grep -v "icon" )
		cd ../
		day=$(echo "$date" | grep -o '..$')
		month=$(echo "$date" | cut -c 2-3)
		year=$(echo "$date" | cut -c 1)
		echo "<table style='display:inline-table'><tr><td><a href=\"$d/index.html\"><img src=\"$d/$fname.icon.JPG\" width=100 height=100></a></td></tr><tr><td> $month-$day-$year </td></tr></table>"
	done >> index.html
	cd ../..
done 
