#!/bin/bash

rm img/index.html
ls -d img/* | while read donor
do
	id=$(echo $donor | sed 's/.*\///')
	numDays=$(cat $donor/index.html | wc -l)
	numPhotos=$(ls $donor/*/ | grep -v "icon" | grep "JPG" | wc -l)
	day=$(ls $donor | grep -v "html" | head -1)
	f=$(ls $donor/$day | grep "icon" | tail -1)
	echo "<table style='display:inline-table'><tr><td><a href=\"$id/index.html\"><img src=\"$id/$day/$f\" width=100 height=100></a></td></tr><tr><td>$id $numDays days</td></tr><tr><td> $numPhotos photos</td></tr></table>" 	
done >> img/index.html
