#!/bin/bash

#create index files for images and html files for curation
ls -d img/* | while read donor
do 
	echo $donor
	cd $donor
	rm *.html
	ls -f *.JPG | grep -v "icon" | while read f
	do 
		fname=$(echo "$f" | sed 's/\.JPG//')
		[[ -f "$fname".icon.JPG ]] || convert "$fname".JPG -resize 100x100 "$fname".icon.JPG
		[[ -f "$fname.html" ]] || cat ../../template.html | sed "s/IMAGE/$fname/" > "$fname.html"
		day=$(echo "$fname" | sed 's/\..*//')
		echo '<a href="'"$fname"'.html"><img src="'"$fname"'.icon.JPG" width=100 height=100></a>' >> "$day".html
		cat index.html | grep "$day.html" > /dev/null || echo '<a href="'"$day"'.html"><img src="'"$fname"'.icon.JPG" width=100 height=100></a>'
	done > index.html
	cd ../..
done 
