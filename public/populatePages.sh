#!/bin/bash

#create index files for images and html files for curation
ls -d 201[1-6]/*/Daily\ Photos/ | while read zz
do 
  cd "$zz"
  rm *.html  
  ls -f *.JPG| grep -v icon.JPG  | grep -v icon2.JPG | while read -r i
  do j=$(echo "$i" | sed 's/\.JPG//')
     [[ -f "$j".icon.JPG ]] || convert "$j".JPG -resize 100x100 "$j".icon.JPG
     [[ -f "$j.html" ]] || cat /data/icputrd/arf/mean.js/public/template.html | sed "s/IMAGE/$j/" > "$j.html"
     k=$(echo "$j" | sed 's|[ (]*[0-9\)]*$||')
     echo '<a href="'"$j"'.html"><img src="'"$j"'.icon.JPG" width=100 height=100></a>'  >> "$k".html
     if [[  "$kp" != "$k" ]]; then n=0; else n=$(($n+1)); fi
     [[ $n == 4 ]] && echo '<a href="'$k'.html"><img src="'"$j"'.icon.JPG" width=100 height=100></a>'
     kp="$k"
  done > index.html
  cd /data/icputrd/arf/mean.js/public 
done 
