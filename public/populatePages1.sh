#!/bin/bash

#update index files for images based on the template
ls -d 201[1-6]/*/Daily\ Photos/ | while read zz
do 
  cd "$zz"
  ls -f *.JPG| grep -v icon.JPG  | grep -v icon2.JPG | while read -r i
  do j=$(echo "$i" | sed 's/\.JPG//')
     cat /data/icputrd/arf/mean.js/public/template.html | sed "s/IMAGE/$j/" > "$j.html"
  done
  cd /data/icputrd/arf/mean.js/public 
done 
