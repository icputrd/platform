#!/usr/bin/python
#python sara_fill_donor_collection.py

import pymongo
import os
import sys
import glob


myclient = pymongo.MongoClient("dbNew")
mydb = myclient["mean-dev"]

mycol = mydb["donors"]

donorList = []

img_path = 'sara_img/'

for donor in glob.glob(img_path + "*"):
        print(donor)
        dayList = []
        if(os.path.isdir(donor)):
                _id = donor[len(img_path):]
                print("\nID: %s" % _id)
                totalPhotos = len([name for name in os.listdir(donor) if "JPG" in name and not "icon" in name])
                print(totalPhotos)

                all_donor_dates = [img[len(donor + "/" + _id):-7] for img in glob.glob(donor + "/*.JPG")]
                date_counts = {}
                for _date in all_donor_dates:
                    if _date in date_counts:
                        date_counts[_date] += 1
                    else:
                        date_counts[_date] = 1
                
                for img in sorted(glob.glob(donor + "/*.JPG")):
                        print(img)
                        date = img[len(donor + "/" + _id):-7]

                        print("Date: %s" % date)
                        if(not "icon" in img):
                                print("Img: %s" % img)
                                num = img.split(".")[1]
                                print("Number: %s" % num)
                                icon = img[:-4] + ".icon.JPG"
                                print("Icon: %s" % icon)
                                html = img[:-3] + "html"
                                print(html)
                                result = next((item for item in dayList if item["date"] == date), None)
                                if (result != None):
                                    result["images"].append( { "num" : num, "path" : img, "icon": icon, "html": html } )
                                else:
                                    dayList.append( { "date": date, "icon": icon, "numPhotos": date_counts[date], "images": [ { "num" : num, "path" : img, "icon": icon, "html": html } ] } )

                donorList.append( { "_id": _id, "icon": icon, "days": dayList, "totalPhotos": totalPhotos, "numDays": len(dayList) } )

mycol.insert_many(donorList)
