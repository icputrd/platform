#!/bin/bash

#create index files for images and html files for curation
ls -d sara_img/* | while read donor
do
    cd /opt/mean.js/public/$donor
    #rm *html
    echo $donor
    ls *icon* | while read f
    do
	    echo $f
	    fname=$(echo "$f" | cut -d "/" -f 3 | sed 's/\.icon.JPG//')
	    [[ -f "$fname.html" ]] || cat /opt/mean.js/public//template.html | sed "s/IMAGE/$fname/" > "$fname.html"
	    echo '<a href="'"$fname"'.html"><img src="'"$fname"'.icon.JPG" width=100 height=100></a>' >> index.html
    done
done
