#!/bin/bash
ls -d 201[1-7]/*/'Daily Photos'*/ |grep -v "Burial"| grep -v "Location"|  while read zz;
do	
	echo "zz: "$zz
	
	oldId=$(echo "$zz" | cut -d'/' -f2)
	startYear=$(echo "$zz" | cut -d'/' -f1 | grep -o '..$')
	newId=$(cat /opt/mean.js/public/sara_all_donors_mappings | grep "$oldId" | cut -d ":" -f 2)
	echo "old_id: "$oldId "new_id: "$newId
	if [ -d "/opt/mean.js/public/sara_img/$newId" ];
	then 
		echo "/opt/mean.js/public/sara_img/$newId exist"
	else
		echo "/opt/mean.js/public/sara_img/$newId does not exist"

		mkdir -p "/opt/mean.js/public/sara_img/"$newId
		cd "/opt/mean.js/public/sara_img/"$newId

		ls "../../$zz" | grep -v "icon" | grep ".JPG" | while read p; 
		do
			echo $p
			utid=$(echo "$p" | sed 's/_.*//') 
			month=$(echo "$p" | cut -d'_' -f2)
			day=$(echo "$p" | cut -d'_' -f3)
			if echo "$p" | grep '(' > /dev/null 
			then
				if echo "$p" | grep " " > /dev/null
				then 
					year=$(echo "$p" |  sed 's/.*_//' | cut -d ' ' -f1 | grep -o '..$')
				else
					year=$(echo "$p" | sed 's/.*_//' | cut -d '(' -f1 | grep -o '..$')
				fi
				number=$(echo "$p" | sed 's/.*(//; s/).*//; s/\..*//')
			else 
				year=$(echo "$p" | sed 's/.*_//;s/\.JPG//' | grep -o '..$')
				number="0"	
			fi

			[[ $oldId != $utid ]] && echo "ID mismatch for $oldId in file $p. $utid instead of $oldId" 
			year=$(echo "$year" | sed 's/\.$//;s/\[$//;s/\]$//;')
			if [ $year -gt $startYear ] 
			then 
				y=$((year - startYear))
			else 
				y=0
			fi 


			[[ $number -lt 10 ]] && number="0$number"
			pre=$(echo "$p" |  sed 's/\..*//')
			ln -s "../../$zz$pre.JPG" "$newId$y$month$day.$number.JPG"
			convert "../../$zz$pre.JPG" -resize 10% "$newId$y$month$day.$number.icon.JPG"
			#if echo "$p" | grep "icon"; then
			#	ln -s "../../$zz$pre.icon.JPG" "$newId$y$month$day.$number.icon.JPG"
			#else
			#	ln -s "../../$zz$pre.JPG" "$newId$y$month$day.$number.JPG"
			#fi
		done
cd ../../
	fi

done
